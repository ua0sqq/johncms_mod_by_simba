<?php defined('_IN_JOHNCMS') or die('Restricted access'); ?>

<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Удалить инфоблок</h2>
            <div class="clearfix"></div>
        </div>
        <div class="alert alert-danger alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                    aria-hidden="true">×</span>
            </button>
            <b>Вы действительно хотите удалить инфоблок?</b><br>
            <p>При удалении будут безвозвратно удалены все свойства и данные инфоблока!</p>
        </div>
        <p>
            <a class="btn btn-danger"
               href="/admin/settings/iblocks/delete/?confirm=1&type=<?= intval($_REQUEST['type']) ?>&id=<?= intval($_REQUEST['id']) ?>">
                Подтвердить удаление
            </a>
            <a class="btn btn-default"
               href="/admin/settings/iblocks/list/?type=<?= intval($_REQUEST['type']) ?>">Отмена</a>
        </p>
        <?
        if (isset($_REQUEST['confirm']) && !empty($_REQUEST['id'])) {
            $blocksRes = new System\Blocks\IBlocks();
            if ($blocksRes->delete($_REQUEST['id'])) {
                header('Location: /admin/settings/iblocks/list/?type=' . intval($_REQUEST['type']));
                exit;
            }
        }
        ?>
    </div>
</div>






