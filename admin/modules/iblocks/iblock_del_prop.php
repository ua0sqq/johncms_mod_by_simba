<?php defined('_IN_JOHNCMS') or die('Restricted access'); ?>

<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Удалить свойство</h2>
            <div class="clearfix"></div>
        </div>
        <div class="alert alert-danger alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                    aria-hidden="true">×</span>
            </button>
            <b>Вы действительно хотите удалить свойство инфобока?</b><br>
            <p>При удалении будут безвозвратно удалены данные, которые содержит это свойство!</p>
        </div>
        <p>
            <a class="btn btn-danger"
               href="/admin/settings/iblocks/delete_prop/?confirm=1&iblock_id=<?= intval($_REQUEST['iblock_id']) ?>&id=<?= intval($_REQUEST['id']) ?>">
                Подтвердить удаление
            </a>
            <a class="btn btn-default"
               href="/admin/settings/iblocks/props/?id=<?= intval($_REQUEST['iblock_id']) ?>">Отмена</a>
        </p>
        <?
        if (isset($_REQUEST['confirm']) && !empty($_REQUEST['id'])) {
            $ibProps = new System\Blocks\IBProps();
            if ($ibProps->delete($_REQUEST['id'])) {
                header('Location: /admin/settings/iblocks/props/?id=' . intval($_REQUEST['iblock_id']));
                exit;
            }
        }
        ?>
    </div>
</div>






