<?php
/**
 * @package     JohnCMS
 * @link        http://johncms.com
 * @copyright   Copyright (C) 2008-2011 JohnCMS Community
 * @license     LICENSE.txt (see attached file)
 * @version     VERSION.txt (see attached file)
 * @author      http://johncms.com/about
 */
use System\Core\DB as DB;

$lng_dl = core::load_lng('downloads');

defined('_IN_JOHNCMS') or die('Error:restricted access');
$textl = $lng_dl['downloads'] . ' - ' . $lng_dl['last_100_files'];
require_once '../incfiles/head.php';
PageBuffer::getInstance()->setTitle($lng_dl['last_100_files'], true);
PageBuffer::getInstance()->addChain($lng_dl['last_100_files'], '/download/index.php?act=new');

$cat = intval($_GET['cat']);
$cat_inf = DB::getInstance()->query("SELECT * FROM `downpath` WHERE `id` = '" . $cat . "' LIMIT 1");

if (DB::getInstance()->numRows($cat_inf)) {
    $cat_inf = DB::getInstance()->getAssoc($cat_inf);
} else {
    $cat_inf = array('way' => '');
}

$totalfile = DB::getInstance()->getCount(DB::getInstance()->query("SELECT COUNT(*) FROM `downfiles`  WHERE `type` != 1 AND `status` = 1 && `way` LIKE '" . $cat_inf['way'] . "%'"),
    0);
if ($totalfile > 100) {
    $totalfile = 100;
}
$zap = DB::getInstance()->query("SELECT * FROM `downfiles` WHERE `type` != 1 AND `status` = 1 && `way` LIKE '" . $cat_inf['way'] . "%' ORDER BY `time` DESC LIMIT " . $start . "," . $kmess);
while ($zap2 = DB::getInstance()->getAssoc($zap)) {
    echo ($i % 2) ? '<div class="list1">' : '<div class="list2">';
    ++$i;
    $tf = pathinfo($zap2['way'], PATHINFO_EXTENSION); // Тип файла
    if ($tf == 'mp3') {
        $set_view = array('variant' => 1, 'way_to_path' => 1);
    } elseif ((($tf == 'thm' or $tf == 'nth') && $down_setting['tmini']) || (($tf == '3gp' or $tf == 'mp4' or $tf == 'avi') && $down_setting['vmini']) || ($tf == 'jpg' or $tf == 'png' or $tf == 'jpeg' or $tf == 'gif')) {
        $set_view = array('link_download' => 1, 'div' => 1, 'way_to_path' => 1);
    } else {
        $set_view = array(
            'variant' => 1,
            'size' => 1,
            'desc' => 1,
            'count' => 1,
            'div' => 1,
            'comments' => 1,
            'add_date' => 1,
            'rating' => 1,
            'way_to_path' => 1,
        );
    }
    echo f_preview($zap2, $set_view, $tf);
    echo '</div>';
}

if ($totalfile < 1) {
    ?>
    <div class="alert alert-dismissible alert-warning">
        <?= $lng['list_empty'] ?>
    </div>
    <?
}


if ($totalfile > $kmess) {
    echo functions::display_pagination('index.php?act=new&amp;cat=' . $cat . '&amp;', $start, $totalfile, $kmess);
}

echo '<div class="menu"><a href="index.html">' . $lng['back'] . '</a></div>';

