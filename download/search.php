<?php
/**
 * @package     JohnCMS
 * @link        http://johncms.com
 * @copyright   Copyright (C) 2008-2011 JohnCMS Community
 * @license     LICENSE.txt (see attached file)
 * @version     VERSION.txt (see attached file)
 * @author      http://johncms.com/about
 *
 * @var $lng_dl
 * @var $lng
 */


define('_IN_JOHNCMS', 1);

$headmod = 'downsearch';
require_once '../incfiles/core.php';
require_once 'functions.php';

use System\Core\DB as DB;

$textl = $lng_dl['downloads'] . ' - ' . $lng_dl['search_files'];
require_once '../incfiles/head.php';
PageBuffer::getInstance()->setTitle($lng_dl['search_files'], true);
PageBuffer::getInstance()->addChain($lng_dl['downloads'], '/download/');
PageBuffer::getInstance()->addChain($lng_dl['search_files'], '/download/search.php');

if (isset($_POST['submit']) || isset($_GET['submit'])) {
    $search = isset ($_POST['search']) ? trim($_POST['query']) : '';
    $search = $search ? $search : rawurldecode(trim($_GET['query']));
    $search = preg_replace("/[^\w\x7F-\xFF\s]/", " ", $search);
    $type = isset ($_POST['search']) ? intval($_POST['search']) : intval($_GET['search']);
    if ($type == 0) {
        $total = DB::getInstance()->getCount(DB::getInstance()->query("SELECT COUNT(*) FROM `downfiles` WHERE MATCH (`desc`) AGAINST ('" . DB::getInstance()->toSql($search) . "')"),
            0);
        $arr = DB::getInstance()->query("SELECT * FROM `downfiles` WHERE MATCH (`desc`) AGAINST ('" . DB::getInstance()->toSql($search) . "')  LIMIT " . $start . ", " . $kmess);
    } else {
        $total = DB::getInstance()->getCount(DB::getInstance()->query("SELECT COUNT(*) FROM `downfiles` WHERE `name` LIKE '%" . DB::getInstance()->toSql($search) . "%'"),
            0);
        $arr = DB::getInstance()->query("SELECT * FROM `downfiles` WHERE `name` LIKE '%" . DB::getInstance()->toSql($search) . "%' LIMIT " . $start . ", " . $kmess);
    }
    if ($total > 0) {
        while ($mass = DB::getInstance()->getAssoc($arr)) {
            echo ($i % 2) ? '<div class="list1">' : '<div class="list2">';
            ++$i;
            $tf = pathinfo($mass['way'], PATHINFO_EXTENSION); // Тип файла
            if ($tf == 'mp3') {
                $set_view = array('variant' => 1, 'way_to_path' => 1);
            } elseif ((($tf == 'thm' or $tf == 'nth') && $down_setting['tmini']) || (($tf == '3gp' or $tf == 'mp4' or $tf == 'avi') && $down_setting['vmini']) || ($tf == 'jpg' or $tf == 'png' or $tf == 'jpeg' or $tf == 'gif')) {
                $set_view = array('link_download' => 1, 'div' => 1, 'way_to_path' => 1);
            } else {
                $set_view = array(
                    'variant' => 1,
                    'size' => 1,
                    'desc' => 1,
                    'count' => 1,
                    'div' => 1,
                    'comments' => 1,
                    'add_date' => 1,
                    'rating' => 1,
                    'way_to_path' => 1,
                );
            }
            echo f_preview($mass, $set_view, $tf);
            echo '</div>';
        }
        echo '<div class="phdr">' . $lng_dl['found'] . ': ' . $total . '</div>';
        if ($total > $kmess) {
            echo '<div class="menu">' . functions::display_pagination('search.php?search=' . $type . '&amp;query=' . $search . '&amp;submit=1&amp;',
                    $start, $total, $kmess) . '</div>';
        }
        echo '<div class="menu"><a href="search.php">' . $lng_dl['new_search'] . '</a><br/>';
        echo '<a href="/download/">' . $lng_dl['back_to_downloads'] . '</a></div>';

    } else {
        echo '<div class="alert alert-dismissible alert-warning">' . $lng_dl['search_not_results'] . '<br/>
    <a href="search.php">' . $lng_dl['repeat'] . '</a></div>';
        echo '<div class="menu"><a href="/download/">' . $lng_dl['back_to_downloads'] . '</a></div>';
    }

} else {
    ?>
    <form action="search.php?" method="post">
        <div class="row">
            <div class="col-xs-12 col-md-8 col-lg-6">

                <label for="query"><?= $lng_dl['search_text'] ?></label>
                <input type="text" value="" id="query" name="query" class="form-control">
                <span class="help-block"><?= $lng_dl['search_text_msg'] ?></span>
                <b><?= $lng_dl['found_for'] ?>:</b><br>
                <label>
                    <input name="search" type="radio" value="0" checked="checked"/> <?= $lng_dl['search_desc'] ?>
                </label><br>
                <label>
                    <input name="search" type="radio" value="1"/> <?= $lng_dl['search_name'] ?>
                </label>
                <br>
                <input class="btn btn-default" type="submit" name="submit" value="<?= $lng_dl['search'] ?>"/>
            </div>
        </div>
    </form>
    <?
    echo '<div class="menu"><a href="/download/">' . $lng_dl['back_to_downloads'] . '</a></div>';
}


require_once '../incfiles/end.php';

