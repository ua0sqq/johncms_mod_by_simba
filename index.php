<?php

/**
 * @package     JohnCMS
 * @link        http://johncms.com
 * @copyright   Copyright (C) 2008-2011 JohnCMS Community
 * @license     LICENSE.txt (see attached file)
 * @version     VERSION.txt (see attached file)
 * @author      http://johncms.com/about
 */

define('_IN_JOHNCMS', 1);

if (isset($_SESSION['ref'])) {
    unset($_SESSION['ref']);
}

$headmod = 'mainpage';
require('incfiles/head.php');
?>
    <section id="services">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="center gap">
                        <h2>Что такое JohnCMS?</h2>
                        <p>Это система управления контентом, оптимизированная для мобильных устройств</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <div class="media">
                        <div class="pull-left">
                            <i class="icon-css3 icon-md"></i>
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">CSS3, HTML5, JS</h3>
                            <p>В шаблонах CMS используются современные технологии для достижения максимального удобства пользования ресурсом</p>
                        </div>
                    </div>
                </div>
                <!--/.col-md-4-->
                <div class="col-md-4 col-sm-6">
                    <div class="media">
                        <div class="pull-left">
                            <i class="icon-mobile-phone icon-md"></i>
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">Адаптивный дизайн</h3>
                            <p>Страницы вашего сайта будут отображаться одинаково хорошо на всех устройствах. Хотя мы и не ограничиваем вашу фантазию</p>
                        </div>
                    </div>
                </div>
                <!--/.col-md-4-->
                <div class="col-md-4 col-sm-6">
                    <div class="media">
                        <div class="pull-left">
                            <i class="icon-terminal icon-md"></i>
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">Легкость использования</h3>

                            <p>Система проста в установке и использовании, и так же не создаёт излишних нагрузок на сервер</p>
                        </div>
                    </div>
                </div>
                <!--/.col-md-4-->
            </div>
            <!--/.row-->
            <div class="gap"></div>
            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <div class="media">
                        <div class="pull-left">
                            <i class="icon-dollar icon-md"></i>
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">Открытая и бесплатная</h3>
                            <p>Система с открытым исходным кодом. Распространяется абсолютно бесплатно.</p>
                        </div>
                    </div>
                </div>
                <!--/.col-md-4-->
                <div class="col-md-4 col-sm-6">
                    <div class="media">
                        <div class="pull-left">
                            <i class="icon-dribbble icon-md"></i>
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">Bootsrtap</h3>
                            <p>
                                В стандартный пакет включен шаблон с использованием css фреймворка bootstrap что даёт вам возможность легко сменить дизайн
                            </p>
                        </div>
                    </div>
                </div>
                <!--/.col-md-4-->
                <div class="col-md-4 col-sm-6">
                    <div class="media">
                        <div class="pull-left">
                            <i class="icon-google-plus icon-md"></i>
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">SEO</h3>

                            <p>Ключевой особенностью данной модификации JohnCMS являются инструменты для оптимизации под поисковые системы и прочий дополнительный функционал</p>
                        </div>
                    </div>
                </div>
                <!--/.col-md-4-->
            </div>
            <!--/.row-->
            <hr>
            <div class="row">
                <div class="col-lg-12">
                    <div class="center">
                        <h2>Что такого в этой модификации?</h2>
                        <p>
                            В первую очередь хотелось бы отметить то, что данная модификация по умолчанию не рассчитана на "старые" мобильные устройства.
                            Но тем не менее блягодаря шаблонной системе вы можете сделать шаблон для старых устройств и с удовольствием пользоваться сайтом.
                            <br>
                            Система содержит значительные доработки, которые позволят вам продвигать Ваш сайт в поисковых системах.
                            А именно, изменена система установки заголовков страницы (title и h1).
                            Теперь вы можете задавать заголовки в любом месте скрипта, что значительно облегчит жизнь разработчикам.
                            То же самое касается и описания страницы и ключевых слов. Они так же с лёгкостью задаются в любой части страницы,
                            что позволит вам создавать абсолютно уникальные описания и ключевые слова для ваших страниц.
                            <br>
                            Появилось такое понятие как навигационная цепочка (более известна как "Хлебные крошки").
                            В стандартном шаблоне внедрена специальная разметка, которая позволит отображать эту цепочку в поисковой выдаче.
                        </p>
                    </div>
                    <div class="gap"></div>
                </div>
            </div>
        </div>
    </section>

    <section id="bottom" class="wet-asphalt links_home">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <h4><?=  $lng['information'] ?></h4>
                    <ul class="arrow">
                        <li><a href="/news/"><?= $lng['news_archive'] ?></a></li>
                        <li><a href="/pages/faq.php"><?= $lng['information'] ?>, FAQ</a></li>
                    </ul>
                </div><!--/.col-md-3-->

                <div class="col-md-3 col-sm-6">
                    <h4><?= $lng['dialogue'] ?></h4>
                    <div>
                        <ul class="arrow">
                            <li><a href="/forum/"><?= $lng['forum'] ?></a> (<?= counters::forum() ?>)</li>
                            <li><a href="/guestbook/"><?= $lng['guestbook'] ?></a> (<?= counters::guestbook() ?>)</li>
                        </ul>
                    </div>
                </div><!--/.col-md-3-->

                <div class="col-md-3 col-sm-6">
                    <h4><?= $lng['useful'] ?></h4>
                    <div>
                        <ul class="arrow">
                            <li><a href="/download/"><?= $lng['downloads'] ?></a> (<?= counters::downloads() ?>)</li>
                            <li><a href="/library/"><?= $lng['library'] ?></a> (<?= counters::library() ?>)</li>
                            <li><a href="/gallery/"><?= $lng['gallery'] ?></a> (<?= counters::gallery() ?>)</li>
                        </ul>
                    </div>
                </div><!--/.col-md-3-->
                <? if ($user_id || $set['active']): ?>
                <div class="col-md-3 col-sm-6">
                    <h4><?= $lng['community'] ?></h4>
                    <div>
                        <ul class="arrow">
                            <li><a href="/users/"><?= $lng['users'] ?></a> (<?= counters::users() ?>)</li>
                            <li><a href="/users/album.php"><?= $lng['photo_albums'] ?></a> (<?= counters::album() ?>)</li>
                        </ul>
                    </div>
                </div> <!--/.col-md-3-->
                <? endif; ?>
            </div>
        </div>
    </section>


<?
require('incfiles/end.php');