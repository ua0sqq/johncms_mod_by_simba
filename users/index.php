<?php

/**
 * @package     JohnCMS
 * @link        http://johncms.com
 * @copyright   Copyright (C) 2008-2011 JohnCMS Community
 * @license     LICENSE.txt (see attached file)
 * @version     VERSION.txt (see attached file)
 * @author      http://johncms.com/about
 */

define('_IN_JOHNCMS', 1);

$headmod = 'users';
require('../incfiles/core.php');
use System\Core\DB as DB;
/*
-----------------------------------------------------------------
Закрываем от неавторизованных юзеров
-----------------------------------------------------------------
*/
if (!$user_id && !$set['active']) {
    require('../incfiles/head.php');
    echo functions::display_error($lng['access_guest_forbidden']);
    require('../incfiles/end.php');
    exit;
}

/*
-----------------------------------------------------------------
Переключаем режимы работы
-----------------------------------------------------------------
*/
$array = array(
    'admlist'  => 'includes',
    'birth'    => 'includes',
    'online'   => 'includes',
    'search'   => 'includes',
    'top'      => 'includes',
    'userlist' => 'includes'
);
$path = !empty($array[$act]) ? $array[$act] . '/' : '';
if (array_key_exists($act, $array) && file_exists($path . $act . '.php')) {
    require_once($path . $act . '.php');
} else {
    /*
    -----------------------------------------------------------------
    Актив сайта
    -----------------------------------------------------------------
    */
    $textl = $lng['community'];
    require('../incfiles/head.php');
    $brth = DB::getInstance()->getCount(DB::getInstance()->query("SELECT COUNT(*) FROM `users` WHERE `dayb` = '" . date('j', time()) . "' AND `monthb` = '" . date('n', time()) . "' AND `preg` = '1'"), 0);
    $count_adm = DB::getInstance()->getCount(DB::getInstance()->query("SELECT COUNT(*) FROM `users` WHERE `rights` > 0"), 0);
    ?>
    <h1><?= $lng['community'] ?></h1>
    <div class="row">
        <div class="col-xs-12 col-md-8 col-lg-6">
            <form action="search.php" method="post">
                <div class="form-group">
                    <label class="control-label"><?= $lng['search'] ?></label>
                    <div class="input-group">
                        <input type="text" class="form-control" name="search"  value="">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="submit"><?= $lng['search'] ?></button>
                    </span>

                    </div>
                    <span class="help-block"><?= $lng['search_nick_help'] ?></span>
                </div>
            </form>

            <ul class="list-group">
                <li class="list-group-item">
                    <a href="index.php?act=userlist"><?= $lng['users'] ?></a>
                    <span class="badge"><?= counters::users() ?></span>
                </li>
                <li class="list-group-item">
                    <a href="index.php?act=admlist"><?= $lng['administration'] ?></a>
                    <span class="badge"><?= $count_adm ?></span>
                </li>
                <? if($brth): ?>
                    <li class="list-group-item">
                        <a href="index.php?act=birth"><?= $lng['birthday_men'] ?></a>
                        <span class="badge"><?= $brth ?></span>
                    </li>
                <? endif; ?>
                <li class="list-group-item">
                    <a href="album.php"><?= $lng['photo_albums'] ?></a>
                    <span class="badge"><?= counters::album() ?></span>
                </li>
                <li class="list-group-item">
                    <a href="index.php?act=top"><?= $lng['users_top'] ?></a>
                </li>
            </ul>

            <p>
                <a href="/"><?= $lng['back'] ?></a>
            </p>

        </div>
    </div>



    <?

}

require_once('../incfiles/end.php');