<?php
/**
 * Created by PhpStorm.
 * User: simba
 * Date: 01.02.2016
 * Time: 23:42
 */
require_once $_SERVER['DOCUMENT_ROOT'] . '/incfiles/head.php';
PageBuffer::getInstance()->setTitle('404, Page not found');
PageBuffer::getInstance()->addHeader('HTTP/1.1 404 Not Found');
?>

    <section id="error" class="container">
        <h2>404, Page not found</h2>
        <p><?= core::$lng['error_404'] ?></p>
        <a class="btn btn-success" href="/"><?= core::$lng['homepage'] ?></a>
    </section>

<?
require_once $_SERVER['DOCUMENT_ROOT'] . '/incfiles/end.php';
?>