<?php

/*
////////////////////////////////////////////////////////////////////////////////
// JohnCMS                Mobile Content Management System                    //
// Project site:          http://johncms.com                                  //
// Support site:          http://gazenwagen.com                               //
////////////////////////////////////////////////////////////////////////////////
// Lead Developer:        Oleg Kasyanov   (AlkatraZ)  alkatraz@gazenwagen.com //
// Development Team:      Eugene Ryabinin (john77)    john77@gazenwagen.com   //
//                        Dmitry Liseenko (FlySelf)   flyself@johncms.com     //
////////////////////////////////////////////////////////////////////////////////
*/

defined('_IN_JOHNCMS') or die('Error: restricted access');

use System\Core\DB as DB;

if ($rights == 3 || $rights >= 6) {
    $topic_vote = DB::getInstance()->getCount(DB::getInstance()->query("SELECT COUNT(*) FROM `cms_forum_vote` WHERE `type`='1' AND `topic` = '$id'"), 0);
    require('../incfiles/head.php');
    if ($topic_vote == 0) {
        echo functions::display_error($lng['error_wrong_data']);
        require('../incfiles/end.php');
        exit;
    }
    if (isset($_GET['yes'])) {
        DB::getInstance()->query("DELETE FROM `cms_forum_vote` WHERE `topic` = '$id'");
        DB::getInstance()->query("DELETE FROM `cms_forum_vote_users` WHERE `topic` = '$id'");
        DB::getInstance()->query("UPDATE `forum` SET  `realid` = '0'  WHERE `id` = '$id'");
        ?>
        <div class="alert alert-dismissible alert-danger">
            <p><?= $lng_forum['voting_deleted'] ?></p>
            <p><a href="<?= $_SESSION['prd'] ?>"><?= $lng['continue'] ?></a></p>
        </div>
        <?
    } else {
        ?>
        <div class="row">
            <div class="col-xs-12">
                <h4><?= $lng_forum['voting_delete_warning'] ?></h4>
                <a href="?act=delvote&amp;id=<?= $id ?>&amp;yes" class="btn-sm btn-danger"><?= $lng['delete'] ?></a>
                <a href="<?= htmlspecialchars(getenv("HTTP_REFERER")) ?>"
                   class="btn-sm btn-default"><?= $lng['cancel'] ?></a>
                <br>
                <br>
            </div>
        </div>
        <?
        $_SESSION['prd'] = htmlspecialchars(getenv("HTTP_REFERER"));
    }
} else {
    header('location: ../index.php?err');
}
