<?php

/**
* @package     JohnCMS
* @link        http://johncms.com
* @copyright   Copyright (C) 2008-2011 JohnCMS Community
* @license     LICENSE.txt (see attached file)
* @version     VERSION.txt (see attached file)
* @author      http://johncms.com/about
*/

defined('_IN_JOHNCMS') or die('Error: restricted access');

use System\Core\DB as DB;

if ($rights == 3 || $rights >= 6) {
    $topic_vote = DB::getInstance()->getCount(DB::getInstance()->query("SELECT COUNT(*) FROM `cms_forum_vote` WHERE `type`='1' AND `topic`='$id'"), 0);
    require('../incfiles/head.php');
    if ($topic_vote == 0) {
        echo functions::display_error($lng['error_wrong_data']);
        require('../incfiles/end.php');
        exit;
    }
    if (isset($_GET['delvote']) && !empty($_GET['vote'])) {
        $vote = abs(intval($_GET['vote']));
        $totalvote = DB::getInstance()->getCount(DB::getInstance()->query("SELECT COUNT(*) FROM `cms_forum_vote` WHERE `type` = '2' AND `id` = '$vote' AND `topic` = '$id'"), 0);
        $countvote = DB::getInstance()->getCount(DB::getInstance()->query("SELECT COUNT(*) FROM `cms_forum_vote` WHERE `type` = '2' AND `topic` = '$id'"), 0);
        if ($countvote <= 2)
            header('location: ?act=editvote&id=' . $id . '');
        if ($totalvote != 0) {
            if (isset($_GET['yes'])) {
                DB::getInstance()->query("DELETE FROM `cms_forum_vote` WHERE `id` = '$vote'");
                $countus = DB::getInstance()->getCount(DB::getInstance()->query("SELECT COUNT(*) FROM `cms_forum_vote_users` WHERE `vote` = '$vote' AND `topic` = '$id'"), 0);
                $topic_vote = DB::getInstance()->getAssoc(DB::getInstance()->query("SELECT `count` FROM `cms_forum_vote` WHERE `type` = '1' AND `topic` = '$id' LIMIT 1"));
                $totalcount = $topic_vote['count'] - $countus;
                DB::getInstance()->query("UPDATE `cms_forum_vote` SET  `count` = '$totalcount'   WHERE `type` = '1' AND `topic` = '$id'");
                DB::getInstance()->query("DELETE FROM `cms_forum_vote_users` WHERE `vote` = '$vote'");
                header('location: ?act=editvote&id=' . $id . '');
            } else {
                echo '<div class="rmenu"><p>' . $lng_forum['voting_variant_warning'] . '<br />' .
                    '<a href="index.php?act=editvote&amp;id=' . $id . '&amp;vote=' . $vote . '&amp;delvote&amp;yes">' . $lng['delete'] . '</a><br />' .
                    '<a href="' . htmlspecialchars(getenv("HTTP_REFERER")) . '">' . $lng['cancel'] . '</a></p></div>';
            }
        } else {
            header('location: ?act=editvote&id=' . $id . '');
        }
    } else if (isset($_POST['submit'])) {
        $vote_name = mb_substr(trim($_POST['name_vote']), 0, 50);
        if (!empty($vote_name))
            DB::getInstance()->query("UPDATE `cms_forum_vote` SET  `name` = '" . DB::getInstance()->toSql($vote_name) . "'  WHERE `topic` = '$id' AND `type` = '1'");
        $vote_result = DB::getInstance()->query("SELECT `id` FROM `cms_forum_vote` WHERE `type`='2' AND `topic`='" . $id . "'");
        while ($vote = DB::getInstance()->getAssoc($vote_result)) {
            if (!empty($_POST[$vote['id'] . 'vote'])) {
                $text = mb_substr(trim($_POST[$vote['id'] . 'vote']), 0, 30);
                DB::getInstance()->query("UPDATE `cms_forum_vote` SET  `name` = '" . DB::getInstance()->toSql($text) . "'  WHERE `id` = '" . $vote['id'] . "'");
            }
        }
        $countvote = DB::getInstance()->getCount(DB::getInstance()->query("SELECT COUNT(*) FROM `cms_forum_vote` WHERE `type`='2' AND `topic`='" . $id . "'"), 0);
        for ($vote = $countvote; $vote < 20; $vote++) {
            if (!empty($_POST[$vote])) {
                $text = mb_substr(trim($_POST[$vote]), 0, 30);
                DB::getInstance()->query("INSERT INTO `cms_forum_vote` SET `name` = '" . DB::getInstance()->toSql($text) . "',  `type` = '2', `topic` = '$id'");
            }
        }
        echo '<div class="gmenu"><p>' . $lng_forum['voting_changed'] . '<br /><a href="index.php?id=' . $id . '">' . $lng['continue'] . '</a></p></div>';
    } else {
        /*
        -----------------------------------------------------------------
        Форма редактирования опроса
        -----------------------------------------------------------------
        */
        PageBuffer::getInstance()->setTitle($lng_forum['edit_vote']);
        PageBuffer::getInstance()->addChain($lng_forum['edit_vote'], '');
        $countvote = DB::getInstance()->getCount(DB::getInstance()->query("SELECT COUNT(*) FROM `cms_forum_vote` WHERE `type` = '2' AND `topic` = '$id'"), 0);
        $topic_vote = DB::getInstance()->getAssoc(DB::getInstance()->query("SELECT `name` FROM `cms_forum_vote` WHERE `type` = '1' AND `topic` = '$id' LIMIT 1"));
        ?>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <form action="index.php?act=editvote&amp;id=<?= $id ?>" method="post" class="">
                    <div class="form-group">
                        <label class="control-label" for="name_vote"><?= $lng_forum['voting'] ?></label>
                        <input name="name_vote" class="form-control" id="name_vote" type="text" value="<?= htmlentities($topic_vote['name'], ENT_QUOTES, 'UTF-8') ?>">
                    </div>
        <?
            $vote_result = DB::getInstance()->query("SELECT `id`, `name` FROM `cms_forum_vote` WHERE `type` = '2' AND `topic` = '$id'");
            while ($vote = DB::getInstance()->getAssoc($vote_result)) {
                ?>
                <div class="form-group">
                    <label class="control-label" for="vote_<?= $vote['id'] ?>"><?= $lng_forum['answer'] ?> <?= ($i + 1) ?> (max. 50)</label>
                    <? if ($countvote > 2): ?>
                        <a href="index.php?act=editvote&amp;id=<?= $id ?>&amp;vote=<?= $vote['id'] ?>&amp;delvote">[x]</a>
                    <? endif; ?>
                    <input name="<?= $vote['id'] ?>" class="form-control" id="vote_<?= $vote['id'] ?>" type="text" value="<?= htmlentities($vote['name'], ENT_QUOTES, 'UTF-8') ?>">
                </div>

                <?
                ++$i;
            }
            if ($countvote < 20) {
                if (isset($_POST['plus']))
                    ++$_POST['count_vote'];
                elseif (isset($_POST['minus']))
                    --$_POST['count_vote'];
                if (empty($_POST['count_vote']))
                    $_POST['count_vote'] = $countvote;
                elseif ($_POST['count_vote'] > 20)
                    $_POST['count_vote'] = 20;
                for ($vote = $i; $vote < $_POST['count_vote']; $vote++) {
                    ?>
                    <div class="form-group">
                        <label class="control-label" for="vote_<?= $vote ?>"><?= $lng_forum['answer'] ?> <?= ($vote + 1) ?> (max. 50)</label>
                        <input name="<?= $vote ?>" class="form-control" id="vote_<?= $vote ?>" type="text" value="<?= functions::checkout($_POST[$vote]) ?>">
                    </div>
                    <?
                }
                echo '<input type="hidden" name="count_vote" value="' . abs(intval($_POST['count_vote'])) . '">' . ($_POST['count_vote'] < 20 ? '<input type="submit" name="plus" value="' . $lng['add'] . '" class="btn btn-default">' : '')
                    . ($_POST['count_vote'] - $countvote ? ' <input type="submit" name="minus" value="' . $lng_forum['delete_last'] . '" class="btn btn-danger">' : '');
            }
            ?>
            </div>
        </div>
        <?

        echo '</p>' .
            '<p><input type="submit" name="submit" value="' . $lng['save'] . '" class="btn btn-success"></p>' .
            '</form>' .
            '<div class="phdr"><a href="index.php?id=' . $id . '">' . $lng['cancel'] . '</a></div>';
    }
}
