<?php

/**
 * @package     JohnCMS
 * @link        http://johncms.com
 * @copyright   Copyright (C) 2008-2011 JohnCMS Community
 * @license     LICENSE.txt (see attached file)
 * @version     VERSION.txt (see attached file)
 * @author      http://johncms.com/about
 */

define('_IN_JOHNCMS', 1);

require_once('../incfiles/core.php');
$lng_forum = core::load_lng('forum');
if (isset($_SESSION['ref'])) {
    unset($_SESSION['ref']);
}
use System\Core\DB as DB;
PageBuffer::getInstance()->addChain(core::$lng['forum'], '/forum/');

/*
-----------------------------------------------------------------
Настройки форума
-----------------------------------------------------------------
*/
$set_forum = $user_id && !empty($datauser['set_forum']) ? unserialize($datauser['set_forum']) : array(
    'farea'    => 0,
    'upfp'     => 0,
    'preview'  => 1,
    'postclip' => 1,
    'postcut'  => 2,
);

/*
-----------------------------------------------------------------
Список расширений файлов, разрешенных к выгрузке
-----------------------------------------------------------------
*/
// Файлы архивов
$ext_arch = array(
    'zip',
    'rar',
    '7z',
    'tar',
    'gz',
    'apk',
);
// Звуковые файлы
$ext_audio = array(
    'mp3',
    'amr',
);
// Файлы документов и тексты
$ext_doc = array(
    'txt',
    'pdf',
    'doc',
    'docx',
    'rtf',
    'djvu',
    'xls',
    'xlsx',
);
// Файлы Java
$ext_java = array(
    'sis',
    'sisx',
    'apk',
);
// Файлы картинок
$ext_pic = array(
    'jpg',
    'jpeg',
    'gif',
    'png',
    'bmp',
);
// Файлы SIS
$ext_sis = array(
    'sis',
    'sisx',
);
// Файлы видео
$ext_video = array(
    '3gp',
    'avi',
    'flv',
    'mpeg',
    'mp4',
);
// Файлы Windows
$ext_win = array(
    'exe',
    'msi',
);
// Другие типы файлов (что не перечислены выше)
$ext_other = array('wmf');

// Ограничиваем доступ к Форуму
$error = '';
if (!$set['mod_forum'] && $rights < 7) {
    $error = $lng_forum['forum_closed'];
} elseif ($set['mod_forum'] == 1 && !$user_id) {
    $error = $lng['access_guest_forbidden'];
}
if ($error) {
    require('../incfiles/head.php');
    echo '<div class="rmenu"><p>' . $error . '</p></div>';
    require('../incfiles/end.php');
    exit;
}

$headmod = $id ? 'forum,' . $id : 'forum';

// Заголовки страниц форума
if (empty($id)) {
    $textl = '' . $lng['forum'] . '';
} else {
    $req = DB::getInstance()->query("SELECT `text` FROM `forum` WHERE `id`= '" . $id . "'");
    $res = DB::getInstance()->getAssoc($req);
    $hdr = strtr($res['text'], array(
        '&laquo;' => '',
        '&raquo;' => '',
        '&quot;'  => '',
        '&amp;'   => '',
        '&lt;'    => '',
        '&gt;'    => '',
        '&#039;'  => '',
    ));
    $hdr = mb_substr($hdr, 0, 30);
    $hdr = functions::checkout($hdr);
    $textl = mb_strlen($res['text']) > 30 ? $hdr . '...' : $hdr;
}

// Переключаем режимы работы
$mods = array(
    'addfile',
    'addvote',
    'close',
    'deltema',
    'delvote',
    'editpost',
    'editvote',
    'file',
    'files',
    'filter',
    'loadtem',
    'massdel',
    'new',
    'nt',
    'per',
    'post',
    'ren',
    'restore',
    'say',
    'tema',
    'users',
    'vip',
    'vote',
    'who',
    'curators',
);
if ($act && ($key = array_search($act, $mods)) !== false && file_exists('includes/' . $mods[$key] . '.php')) {
    require('includes/' . $mods[$key] . '.php');
} else {
    require('../incfiles/head.php');

    // Если форум закрыт, то для Админов выводим напоминание
    if (!$set['mod_forum']) {
        echo '<div class="alarm">' . $lng_forum['forum_closed'] . '</div>';
    } elseif ($set['mod_forum'] == 3) {
        echo '<div class="rmenu">' . $lng['read_only'] . '</div>';
    }
    if (!$user_id) {
        if (isset($_GET['newup'])) {
            $_SESSION['uppost'] = 1;
        }
        if (isset($_GET['newdown'])) {
            $_SESSION['uppost'] = 0;
        }
    }
    if ($id) {
        // Определяем тип запроса (каталог, или тема)
        $type = DB::getInstance()->query("SELECT * FROM `forum` WHERE `id`= '$id'");
        if (!DB::getInstance()->numRows($type)) {
            // Если темы не существует, показываем ошибку
            echo functions::display_error($lng_forum['error_topic_deleted'],
                '<a href="index.php">' . $lng['to_forum'] . '</a>');
            require('../incfiles/end.php');
            exit;
        }
        $type1 = DB::getInstance()->getAssoc($type);

        // Фиксация факта прочтения Топика
        if ($user_id && $type1['type'] == 't') {
            $req_r = DB::getInstance()->query("SELECT * FROM `cms_forum_rdm` WHERE `topic_id` = '$id' AND `user_id` = '$user_id' LIMIT 1");
            if (DB::getInstance()->numRows($req_r)) {
                $res_r = DB::getInstance()->getAssoc($req_r);
                if ($type1['time'] > $res_r['time']) {
                    DB::getInstance()->query("UPDATE `cms_forum_rdm` SET `time` = '" . time() . "' WHERE `topic_id` = '$id' AND `user_id` = '$user_id' LIMIT 1");
                }
            } else {
                DB::getInstance()->query("INSERT INTO `cms_forum_rdm` SET `topic_id` = '$id', `user_id` = '$user_id', `time` = '" . time() . "'");
            }
        }

        // Получаем структуру форума
        $res = true;
        $allow = 0;
        $parent = $type1['refid'];
        while ($parent != '0' && $res != false) {
            $req = DB::getInstance()->query("SELECT * FROM `forum` WHERE `id` = '$parent' LIMIT 1");
            $res = DB::getInstance()->getAssoc($req);
            if ($res['type'] == 'f' || $res['type'] == 'r') {
                //$tree[] = '<a href="index.php?id=' . $parent . '">' . $res['text'] . '</a>';
                $tree[] = array('index.php?id=' . $parent . '', $res['text']);
                if ($res['type'] == 'r' && !empty($res['edit'])) {
                    $allow = intval($res['edit']);
                }
            }
            $parent = $res['refid'];
        }
        if(is_array($tree)) {
            krsort($tree);
        }
        if ($type1['type'] != 'm') {
            $tree[] = array('index.php?id=' . $type1['id'] . '', $type1['text']);
        }
        foreach($tree as $key=>$item) {
            PageBuffer::getInstance()->addChain($item['1'], '/forum/'.$item['0']);
        }

        // Счетчик файлов и ссылка на них
        $sql = ($rights == 9) ? "" : " AND `del` != '1'";
        if ($type1['type'] == 'f') {
            $count = DB::getInstance()->getCount(DB::getInstance()->query("SELECT COUNT(*) FROM `cms_forum_files` WHERE `cat` = '$id'" . $sql));
            if ($count > 0) {
                $filelink = '<a href="index.php?act=files&amp;c=' . $id . '">' . $lng_forum['files_category'] . '&#160;<span class="badge">' . $count . '</span></a>';
            }
        } elseif ($type1['type'] == 'r') {
            $count = DB::getInstance()->getCount(DB::getInstance()->query("SELECT COUNT(*) FROM `cms_forum_files` WHERE `subcat` = '$id'" . $sql));
            if ($count > 0) {
                $filelink = '<a href="index.php?act=files&amp;s=' . $id . '">' . $lng_forum['files_section'] . '&#160;<span class="badge">' . $count . '</span></a>';
            }
        } elseif ($type1['type'] == 't') {
            $count = DB::getInstance()->getCount(DB::getInstance()->query("SELECT COUNT(*) FROM `cms_forum_files` WHERE `topic` = '$id'" . $sql));
            if ($count > 0) {
                $filelink = '<a href="index.php?act=files&amp;t=' . $id . '">' . $lng_forum['files_topic'] . '&#160;<span class="badge">' . $count . '</span></a>';
            }
        }


        // Счетчик "Кто в теме?"
        $wholink = false;
        if ($user_id && $type1['type'] == 't') {
            $online_u = DB::getInstance()->getCount(DB::getInstance()->query("SELECT COUNT(*) FROM `users` WHERE `lastdate` > " . (time() - 300) . " AND `place` = 'forum,$id'"));
            $online_g = DB::getInstance()->getCount(DB::getInstance()->query("SELECT COUNT(*) FROM `cms_sessions` WHERE `lastdate` > " . (time() - 300) . " AND `place` = 'forum,$id'"));
            $wholink = '<a href="index.php?act=who&amp;id=' . $id . '">' . $lng_forum['who_here'] . '? <span class="badge">' . $online_u . '&#160;/&#160;' . $online_g . '</span></a>';
        }


        ?>
        <ul class="nav nav-pills">
            <li>
                <?= counters::forum_new(1) ?>
            </li>
            <li>
                <a href="search.php?id=<?= $id ?>"><?= $lng['search'] ?></a>
            </li>
            <? if ($filelink): ?>
                <li>
                    <?= $filelink ?>
                </li>
            <? endif; ?>
            <? if ($wholink): ?>
                <li>
                    <?= $wholink ?>
                </li>
            <? endif; ?>
        </ul>
        <?

        PageBuffer::getInstance()->setTitle(core::$lng['forum'] . ' - ' . $textl);
        PageBuffer::getInstance()->setTitle($textl, true);
        switch ($type1['type']) {
            case 'f':
                ////////////////////////////////////////////////////////////
                // Список разделов форума                                 //
                ////////////////////////////////////////////////////////////
                $req = DB::getInstance()->query("SELECT `id`, `text`, `soft`, `edit` FROM `forum` WHERE `type`='r' AND `refid`='$id' ORDER BY `realid`");
                $total = DB::getInstance()->numRows($req);
                if ($total) {
                    $i = 0;
                    while ($res = DB::getInstance()->getAssoc($req)) {
                        echo $i % 2 ? '<div class="list2 forum_section">' : '<div class="list1 forum_section">';
                        $coltem = DB::getInstance()->getCount(DB::getInstance()->query("SELECT COUNT(*) FROM `forum` WHERE `type` = 't' AND `refid` = '" . $res['id'] . "'"));
                        echo '<a href="?id=' . $res['id'] . '">' . $res['text'] . '</a>';
                        if ($coltem) {
                            echo " [$coltem]";
                        }
                        if (!empty($res['soft'])) {
                            echo '<div class="sub"><span class="gray">' . $res['soft'] . '</span></div>';
                        }
                        echo '</div>';
                        ++$i;
                    }
                    unset($_SESSION['fsort_id']);
                    unset($_SESSION['fsort_users']);
                } else {
                    echo '<div class="alert alert-dismissible alert-warning"><p>' . $lng_forum['section_list_empty'] . '</p></div>';
                }
                echo '<div class="phdr">' . $lng['total'] . ': ' . $total . '</div>';
                break;

            case 'r':
                ////////////////////////////////////////////////////////////
                // Список топиков                                         //
                ////////////////////////////////////////////////////////////
                $total = DB::getInstance()->getCount(DB::getInstance()->query("SELECT COUNT(*) FROM `forum` WHERE `type`='t' AND `refid`='$id'" . ($rights >= 7 ? '' : " AND `close`!='1'")));
                if (($user_id && !isset($ban['1']) && !isset($ban['11']) && $set['mod_forum'] != 4) || core::$user_rights) {
                    // Кнопка создания новой темы
                    ?>
                    <a href="index.php?act=nt&amp;id=<?= $id ?>" class="btn btn-success"><?= $lng_forum['new_topic'] ?></a>
                    <div style="padding: 5px;"></div>
                    <?
                }
                if ($total) {
                    $req = DB::getInstance()->query("SELECT * FROM `forum` WHERE `type`='t'" . ($rights >= 7 ? '' : " AND `close`!='1'") . " AND `refid`='$id' ORDER BY `vip` DESC, `time` DESC LIMIT $start, $kmess");
                    $i = 0;
                    while ($res = DB::getInstance()->getAssoc($req)) {
                        if ($res['close']) {
                            echo '<div class="rmenu">';
                        } else {
                            echo $i % 2 ? '<div class="list2">' : '<div class="list1">';
                        }
                        $nikuser = DB::getInstance()->query("SELECT `from` FROM `forum` WHERE `type` = 'm' AND `close` != '1' AND `refid` = '" . $res['id'] . "' ORDER BY `time` DESC LIMIT 1");
                        $nam = DB::getInstance()->getAssoc($nikuser);
                        $colmes = DB::getInstance()->query("SELECT COUNT(*) FROM `forum` WHERE `type`='m' AND `refid`='" . $res['id'] . "'" . ($rights >= 7 ? '' : " AND `close` != '1'"));
                        $colmes1 = DB::getInstance()->getCount($colmes);
                        $cpg = ceil($colmes1 / $kmess);
                        $np = DB::getInstance()->getCount(DB::getInstance()->query("SELECT COUNT(*) FROM `cms_forum_rdm` WHERE `time` >= '" . $res['time'] . "' AND `topic_id` = '" . $res['id'] . "' AND `user_id`='$user_id'"));
                        // Значки
                        $icons = array(
                            ($np ? (!$res['vip'] ? functions::image('op.gif') : '') : functions::image('np.gif')),
                            ($res['vip'] ? functions::image('pt.gif') : ''),
                            ($res['realid'] ? functions::image('rate.gif') : ''),
                            ($res['edit'] ? functions::image('tz.gif') : ''),
                        );
                        echo functions::display_menu($icons, '');
                        echo '<a href="index.php?id=' . $res['id'] . '">' . $res['text'] . '</a> [' . $colmes1 . ']';
                        if ($cpg > 1) {
                            echo '<a href="index.php?id=' . $res['id'] . '&amp;page=' . $cpg . '">&#160;&gt;&gt;</a>';
                        }
                        echo '<div class="sub">';
                        echo $res['from'];
                        if (!empty($nam['from'])) {
                            echo '&#160;/&#160;' . $nam['from'];
                        }
                        echo ' <span class="gray">(' . functions::display_date($res['time']) . ')</span></div></div>';
                        ++$i;
                    }
                    unset($_SESSION['fsort_id']);
                    unset($_SESSION['fsort_users']);
                } else {
                    echo '<div class="alert alert-dismissible alert-warning"><p>' . $lng_forum['topic_list_empty'] . '</p></div>';
                }
                echo '<div class="phdr">' . $lng['total'] . ': ' . $total . '</div>';
                if ($total > $kmess) {
                    echo '<div class="topmenu">' . functions::display_pagination('index.php?id=' . $id . '&amp;',
                            $start, $total, $kmess) . '</div>' .
                        '<p><form action="index.php?id=' . $id . '" method="post">' .
                        '<input type="text" name="page" size="2"/>' .
                        '<input type="submit" value="' . $lng['to_page'] . ' &gt;&gt;"/>' .
                        '</form></p>';
                }
                break;

            case 't':
                ////////////////////////////////////////////////////////////
                // Показываем тему с постами                              //
                ////////////////////////////////////////////////////////////
                $filter = isset($_SESSION['fsort_id']) && $_SESSION['fsort_id'] == $id ? 1 : 0;
                $sql = '';
                if ($filter && !empty($_SESSION['fsort_users'])) {
                    // Подготавливаем запрос на фильтрацию юзеров
                    $sw = 0;
                    $sql = ' AND (';
                    $fsort_users = unserialize($_SESSION['fsort_users']);
                    foreach ($fsort_users as $val) {
                        if ($sw) {
                            $sql .= ' OR ';
                        }
                        $sortid = intval($val);
                        $sql .= "`forum`.`user_id` = '$sortid'";
                        $sw = 1;
                    }
                    $sql .= ')';
                }

                // Если тема помечена для удаления, разрешаем доступ только администрации
                if ($rights < 6 && $type1['close'] == 1) {
                    echo '<div class="alert alert-dismissible alert-danger"><p>' . $lng_forum['topic_deleted'] . '<br/><a href="?id=' . $type1['refid'] . '">' . $lng_forum['to_section'] . '</a></p></div>';
                    require('../incfiles/end.php');
                    exit;
                }

                // Счетчик постов темы
                $colmes = DB::getInstance()->getCount(DB::getInstance()->query("SELECT COUNT(*) FROM `forum` WHERE `type`='m'$sql AND `refid`='$id'" . ($rights >= 7 ? '' : " AND `close` != '1'")));
                if ($start >= $colmes) {
                    // Исправляем запрос на несуществующую страницу
                    $start = max(0, $colmes - (($colmes % $kmess) == 0 ? $kmess : ($colmes % $kmess)));
                }


                if ($colmes > $kmess) {
                    echo functions::display_pagination('index.php?id=' . $id . '&amp;',
                        $start, $colmes, $kmess);
                }

                // Метка удаления темы
                if ($type1['close']) {
                    echo '<div class="alert alert-dismissible alert-danger">' . $lng_forum['topic_delete_who'] . ': <b>' . $type1['close_who'] . '</b></div>';
                } elseif (!empty($type1['close_who']) && $rights >= 7) {
                    echo '<div class="gmenu"><small>' . $lng_forum['topic_delete_whocancel'] . ': <b>' . $type1['close_who'] . '</b></small></div>';
                }

                // Метка закрытия темы
                if ($type1['edit']) {
                    echo '<div class="alert alert-dismissible alert-danger">' . $lng_forum['topic_closed'] . '</div>';
                }

                // Блок голосований
                if ($type1['realid']) {
                    $clip_forum = isset($_GET['clip']) ? '&amp;clip' : '';
                    $vote_user = DB::getInstance()->getCount(DB::getInstance()->query("SELECT COUNT(*) FROM `cms_forum_vote_users` WHERE `user`='$user_id' AND `topic`='$id'"));
                    $topic_vote = DB::getInstance()->getAssoc(DB::getInstance()->query("SELECT `name`, `time`, `count` FROM `cms_forum_vote` WHERE `type`='1' AND `topic`='$id' LIMIT 1"));
                    ?>
                    <div class="panel panel-default">
                      <div class="panel-heading"><?= functions::checkout($topic_vote['name']) ?></div>
                      <div class="panel-body">


<?
                    $vote_result = DB::getInstance()->query("SELECT `id`, `name`, `count` FROM `cms_forum_vote` WHERE `type`='2' AND `topic`='" . $id . "' ORDER BY `id` ASC");
                    if (!$type1['edit'] && !isset($_GET['vote_result']) && $user_id && $vote_user == 0) {
                        // Выводим форму с опросами
                        echo '<form action="index.php?act=vote&amp;id=' . $id . '" method="post">';
                        while ($vote = DB::getInstance()->getAssoc($vote_result)) {
                        ?>
                            <label>
                            <input type="radio" value="<?= $vote['id'] ?>" name="vote"/>
                            <?= functions::checkout($vote['name'], 0, 1) ?>
                            </label><br>
                        <?
                        } ?>
                        <input class="btn btn-default btn-sm" type="submit" name="submit" value="<?= $lng['vote'] ?>"/>
                        <?
                        echo '<p><br /><a href="index.php?id=' . $id . '&amp;start=' . $start . '&amp;vote_result' . $clip_forum .
                            '">' . $lng_forum['results'] . '</a></p></form>';
                    } else {
                        // Выводим результаты голосования


                        echo '<small>';
                        while ($vote = DB::getInstance()->getAssoc($vote_result)) {
                            $count_vote = $topic_vote['count'] ? round(100 / $topic_vote['count'] * $vote['count']) : 0;
                            echo functions::checkout($vote['name'], 0, 1) . ' [' . $vote['count'] . ']<br />';
                            echo '<img src="vote_img.php?img=' . $count_vote . '" alt="' . $lng_forum['rating'] . ': ' . $count_vote . '%" /><br />';
                        }
                        echo '</small>' . $lng_forum['total_votes'] . ': ';
                        if (core::$user_rights > 6) {
                            echo '<a href="index.php?act=users&amp;id=' . $id . '">' . $topic_vote['count'] . '</a>';
                        } else {
                            echo $topic_vote['count'];
                        }

                        if ($user_id && $vote_user == 0) {
                            echo '<div class="bmenu"><a href="index.php?id=' . $id . '&amp;start=' . $start . $clip_forum . '">' . $lng['vote'] . '</a></div>';
                        }
                    } ?>
                      </div>
                    </div>
                    <?
                }

                // Получаем данные о кураторах темы
                $curators = !empty($type1['curators']) ? unserialize($type1['curators']) : array();
                $curator = false;
                if ($rights < 6 && $rights != 3 && $user_id) {
                    if (array_key_exists($user_id, $curators)) {
                        $curator = true;
                    }
                }

                // Фиксация первого поста в теме
                if (($set_forum['postclip'] == 2 && ($set_forum['upfp'] ? $start < (ceil($colmes - $kmess)) : $start > 0)) || isset($_GET['clip'])) {
                    $postreq = DB::getInstance()->query("SELECT `forum`.*, `users`.`sex`, `users`.`rights`, `users`.`lastdate`, `users`.`status`, `users`.`datereg`
                    FROM `forum` LEFT JOIN `users` ON `forum`.`user_id` = `users`.`id`
                    WHERE `forum`.`type` = 'm' AND `forum`.`refid` = '$id'" . ($rights >= 7 ? "" : " AND `forum`.`close` != '1'") . "
                    ORDER BY `forum`.`id` LIMIT 1");
                    $postres = DB::getInstance()->getAssoc($postreq);
                    echo '<div class="list2 forum_post"><p>';
                    if ($postres['sex']) {
                    echo functions::image(($postres['sex'] == 'm' ? 'm' : 'w') . ($postres['datereg'] > time() - 86400 ? '_new' : '') . '.png',
                                        array('class' => 'icon-inline')).'&#160;';

                    } else {
                        echo '<img src="../images/del.png" width="10" height="10" alt=""/>&#160;';
                    }
                    if ($user_id && $user_id != $postres['user_id']) {
                        echo '<a href="../users/profile.php?user=' . $postres['user_id'] . '&amp;fid=' . $postres['id'] . '"><b class="user_login">' . $postres['from'] . '</b></a> ' .
                            '<a href="index.php?act=say&amp;id=' . $postres['id'] . '&amp;start=' . $start . '"> ' . $lng_forum['reply_btn'] . '</a> ' .
                            '<a href="index.php?act=say&amp;id=' . $postres['id'] . '&amp;start=' . $start . '&amp;cyt"> ' . $lng_forum['cytate_btn'] . '</a> ';
                    } else {
                        echo '<b class="user_login">' . $postres['from'] . '</b> ';
                    }
                    $user_rights = array(
                        3 => '(FMod)',
                        6 => '(Smd)',
                        7 => '(Adm)',
                        9 => '(SV!)',
                    );
                    echo @$user_rights[$postres['rights']];
                    echo(time() > $postres['lastdate'] + 300 ? '<span class="red"> [Off]</span>' : '<span class="green"> [ON]</span>');
                    echo ' <span class="gray">(' . functions::display_date($postres['time']) . ')</span><br/>';
                    if ($postres['close']) {
                        echo '<span class="red">' . $lng_forum['post_deleted'] . '</span><br/>';
                    }
                    echo functions::checkout(mb_substr($postres['text'], 0, 500), 0, 2);
                    if (mb_strlen($postres['text']) > 500) {
                        echo '...<a href="index.php?act=post&amp;id=' . $postres['id'] . '">' . $lng_forum['read_all'] . '</a>';
                    }
                    echo '</p></div>';
                }

                // Памятка, что включен фильтр
                if ($filter) {
                    echo '<div class="rmenu">' . $lng_forum['filter_on'] . '</div>';
                }

                // Задаем правила сортировки (новые внизу / вверху)
                if ($user_id) {
                    $order = $set_forum['upfp'] ? 'DESC' : 'ASC';
                } else {
                    $order = ((empty($_SESSION['uppost'])) || ($_SESSION['uppost'] == 0)) ? 'ASC' : 'DESC';
                }

                ////////////////////////////////////////////////////////////
                // Основной запрос в базу, получаем список постов темы    //
                ////////////////////////////////////////////////////////////
                $req = DB::getInstance()->query("
                  SELECT `forum`.*, `users`.`sex`, `users`.`rights`, `users`.`lastdate`, `users`.`status`, `users`.`datereg`
                  FROM `forum` LEFT JOIN `users` ON `forum`.`user_id` = `users`.`id`
                  WHERE `forum`.`type` = 'm' AND `forum`.`refid` = '$id'"
                    . ($rights >= 7 ? "" : " AND `forum`.`close` != '1'") . "$sql
                  ORDER BY `forum`.`id` $order LIMIT $start, $kmess
                ");

                // Верхнее поле "Написать"
                if (($user_id && !$type1['edit'] && $set_forum['upfp'] && $set['mod_forum'] != 3 && $allow != 4) || ($rights >= 7 && $set_forum['upfp'])) {
                    echo '<div class="gmenu"><form name="form1" action="index.php?act=say&amp;id=' . $id . '" method="post">';
                    if ($set_forum['farea']) {
                        $token = mt_rand(1000, 100000);
                        $_SESSION['token'] = $token;
                        echo '<p>' .
                            bbcode::auto_bb('form1', 'msg') .
                            '<textarea rows="' . $set_user['field_h'] . '" name="msg"></textarea></p>' .
                            '<p><input type="checkbox" name="addfiles" value="1" /> ' . $lng_forum['add_file'] .
                            ($set_user['translit'] ? '<br /><input type="checkbox" name="msgtrans" value="1" /> ' . $lng['translit'] : '') .
                            '</p><p><input type="submit" name="submit" value="' . $lng['write'] . '" style="width: 107px; cursor: pointer;"/> ' .
                            (isset($set_forum['preview']) && $set_forum['preview'] ? '<input type="submit" value="' . $lng['preview'] . '" style="width: 107px; cursor: pointer;"/>' : '') .
                            '<input type="hidden" name="token" value="' . $token . '"/>' .
                            '</p></form></div>';
                    } else {
                        echo '<p><input type="submit" name="submit" value="' . $lng['write'] . '"/></p></form></div>';
                    }
                }

                // Для администрации включаем форму массового удаления постов
                if ($rights == 3 || $rights >= 6) {
                    echo '<form action="index.php?act=massdel" method="post">';
                }
                $i = 1;

                ////////////////////////////////////////////////////////////
                // Основной список постов                                 //
                ////////////////////////////////////////////////////////////
                while ($res = DB::getInstance()->getAssoc($req)) {
                    // Фон поста
                    if ($res['close']) {
                        echo '<div class="alert alert-dismissible alert-danger forum_post">';
                    } else {
                        echo $i % 2 ? '<div class="list2 forum_post">' : '<div class="list1 forum_post">';
                    }

                    // Пользовательский аватар

                    // Метка должности
                    $user_rights = array(
                        3 => 'FMod',
                        6 => 'Smd',
                        7 => 'Adm',
                        9 => 'SV!',
                    );
                    ?>

                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <div class="avatar">
                                    <?= (isset($user_rights[$res['rights']]) ? '<span class="post label label-info">' . $user_rights[$res['rights']] . '</span>' : '') ?>
                                    <?= (time() > $res['lastdate'] + 300 ? '<span class="status label label-danger">off</span>' : '<span class="status label label-success">on</span>') ?>
                                    <? if (file_exists(('../files/users/avatar/' . $res['user_id'] . '.png'))): ?>
                                        <img src="../files/users/avatar/<?= $res['user_id'] ?>.png" width="60"
                                             height="60"
                                             alt="<?= $res['from'] ?>"/>&#160;
                                    <? else: ?>
                                        <img src="/images/icon-user-default.png" width="60" height="60"
                                             alt="<?= $res['from'] ?>"/>&#160;
                                    <? endif; ?>
                                </div>
                            </td>
                            <td>
                                <!-- Пол -->
                                <? if ($res['sex']): ?>
                                    <?= functions::image(($res['sex'] == 'm' ? 'm' : 'w') . ($res['datereg'] > time() - 86400 ? '_new' : '') . '.png',
                                        array('class' => 'icon-inline')) ?>
                                <? else: ?>
                                    <?= functions::image('del.png', array('class' => 'icon-inline')) ?>
                                <? endif; ?>

                                <!-- Ник юзера и ссылка на его анкету -->

                                <? if ($user_id && $user_id != $res['user_id']): ?>
                                    <a href="../users/profile.php?user=<?= $res['user_id'] ?>"><b
                                            class="user_login"><?= $res['from'] ?></b></a>
                                <? else: ?>
                                    <b class="user_login"><?= $res['from'] ?></b>
                                <? endif;

                                // Ссылка на пост
                                echo '<a href="index.php?act=post&amp;id=' . $res['id'] . '" title="Link to post">[#]</a>';

                                // Ссылки на ответ и цитирование
                                if ($user_id && $user_id != $res['user_id']) {
                                    echo '&#160;<a href="index.php?act=say&amp;id=' . $res['id'] . '&amp;start=' . $start . '">' . $lng_forum['reply_btn'] . '</a>&#160;' .
                                        '<a href="index.php?act=say&amp;id=' . $res['id'] . '&amp;start=' . $start . '&amp;cyt">' . $lng_forum['cytate_btn'] . '</a> ';
                                }

                                // Время поста
                                echo ' <span class="post_date gray">(' . functions::display_date($res['time']) . ')</span><br />';

                                // Статус пользователя
                                if (!empty($res['status'])) {
                                    echo '<div class="label label-warning">' . $res['status'] . '</div>';
                                }
                                ?>
                            </td>
                        </tr>
                    </table>
                    <?

                    ////////////////////////////////////////////////////////////
                    // Вывод текста поста                                     //
                    ////////////////////////////////////////////////////////////
                    $text = $res['text'];
                    $text = functions::checkout($text, 1, 1);
                    if ($set_user['smileys']) {
                        $text = functions::smileys($text, $res['rights'] ? 1 : 0);
                    }
                    ?>
                    <div class="post_text">
                        <?= $text ?>
                    </div>

                    <?
                    // Если пост редактировался, показываем кем и когда
                    if ($res['kedit']) {
                        echo '<br /><span class="gray"><small>' . $lng_forum['edited'] . ' <b>' . $res['edit'] . '</b> (' . functions::display_date($res['tedit']) . ') <b>[' . $res['kedit'] . ']</b></small></span>';
                    }

                    // Если есть прикрепленный файл, выводим его описание
                    $freq = DB::getInstance()->query("SELECT * FROM `cms_forum_files` WHERE `post` = '" . $res['id'] . "'");
                    if (DB::getInstance()->numRows($freq) > 0) {
                        $fres = DB::getInstance()->getAssoc($freq);
                        $fls = round(@filesize('../files/forum/attach/' . $fres['filename']) / 1024, 2);
                        echo '<div class="gray" style="background-color: rgba(128, 128, 128, 0.1); padding: 2px 4px; margin-top: 4px">' . $lng_forum['attached_file'] . ':';
                        // Предпросмотр изображений
                        $att_ext = strtolower(functions::format('./files/forum/attach/' . $fres['filename']));
                        $pic_ext = array(
                            'gif',
                            'jpg',
                            'jpeg',
                            'png',
                        );
                        if (in_array($att_ext, $pic_ext)) {
                            echo '<div><a href="index.php?act=file&amp;id=' . $fres['id'] . '" target="_blank">';
                            echo '<img src="thumbinal.php?file=' . (urlencode($fres['filename'])) . '" alt="' . $lng_forum['click_to_view'] . '" /></a></div>';
                        } else {
                            echo '<br /><a href="index.php?act=file&amp;id=' . $fres['id'] . '" target="_blank">' . $fres['filename'] . '</a>';
                        }
                        echo ' (' . $fls . ' кб.)<br/>';
                        echo $lng_forum['downloads'] . ': ' . $fres['dlcount'] . ' ' . $lng_forum['time'] . '</div>';
                        $file_id = $fres['id'];
                    }

                    // Ссылки на редактирование / удаление постов
                    if (
                        (($rights == 3 || $rights >= 6 || $curator) && $rights >= $res['rights'])
                        || ($res['user_id'] == $user_id && !$set_forum['upfp'] && ($start + $i) == $colmes && $res['time'] > time() - 300)
                        || ($res['user_id'] == $user_id && $set_forum['upfp'] && $start == 0 && $i == 1 && $res['time'] > time() - 300)
                        || ($i == 1 && $allow == 2 && $res['user_id'] == $user_id)
                    ) {
                        echo '<div class="sub">';

                        // Чекбокс массового удаления постов
                        if ($rights == 3 || $rights >= 6) {
                            echo '<label class="check_post"><input type="checkbox" name="delch[]" value="' . $res['id'] . '"/>&#160;</label>';
                        }

                        // Служебное меню поста
                        $menu = array(
                            '<a href="index.php?act=editpost&amp;id=' . $res['id'] . '">' . $lng['edit'] . '</a>',
                            ($rights >= 7 && $res['close'] == 1 ? '<a href="index.php?act=editpost&amp;do=restore&amp;id=' . $res['id'] . '">' . $lng_forum['restore'] . '</a>' : ''),
                            ($res['close'] == 1 ? '' : '<a href="index.php?act=editpost&amp;do=del&amp;id=' . $res['id'] . '">' . $lng['delete'] . '</a>'),
                        );
                        echo functions::display_menu($menu);

                        // Показываем, кто удалил пост
                        if ($res['close']) {
                            echo '<div class="red">' . $lng_forum['who_delete_post'] . ': <b>' . $res['close_who'] . '</b></div>';
                        } elseif (!empty($res['close_who'])) {
                            echo '<div class="green">' . $lng_forum['who_restore_post'] . ': <b>' . $res['close_who'] . '</b></div>';
                        }

                        // Показываем IP и Useragent
                        if ($rights == 3 || $rights >= 6) {
                            if ($res['ip_via_proxy']) {
                                echo '<div class="gray browser"><b class="red"><a href="' . $set['homeurl'] . '/' . $set['admp'] . '/index.php?act=search_ip&amp;ip=' . long2ip($res['ip']) . '">' . long2ip($res['ip']) . '</a></b> - ' .
                                    '<a href="' . $set['homeurl'] . '/' . $set['admp'] . '/index.php?act=search_ip&amp;ip=' . long2ip($res['ip_via_proxy']) . '">' . long2ip($res['ip_via_proxy']) . '</a>' .
                                    ' - ' . $res['soft'] . '</div>';
                            } else {
                                echo '<div class="gray browser"><a href="' . $set['homeurl'] . '/' . $set['admp'] . '/index.php?act=search_ip&amp;ip=' . long2ip($res['ip']) . '">' . long2ip($res['ip']) . '</a> - ' . $res['soft'] . '</div>';
                            }
                        }
                        echo '</div>';
                    }
                    echo '</div>';
                    ++$i;
                }

                // Кнопка массового удаления постов
                if ($rights == 3 || $rights >= 6) {
                    echo '<input class="btn btn-danger btn-sm" type="submit" value=" ' . $lng['delete'] . ' "/>';
                    echo '</form><br>';
                }

                // Нижнее поле "Написать"
                if (($user_id && !$type1['edit'] && !$set_forum['upfp'] && $set['mod_forum'] != 3 && $allow != 4) || ($rights >= 7 && !$set_forum['upfp'])) {
                    echo '<form name="form2" action="index.php?act=say&amp;id=' . $id . '" method="post">';
                    if ($set_forum['farea']) {
                        $token = mt_rand(1000, 100000);
                        $_SESSION['token'] = $token;
                        ?>
                        <textarea rows="<?= $set_user['field_h'] ?>" name="msg" class="bb_editor"></textarea>
                        <label class="forum_add_file"><input type="checkbox" name="addfiles" value="1" /> <?= $lng_forum['add_file']?></label>
                        <br>
                        <input class="btn btn-success btn-sm" type="submit" name="submit" value="<?= $lng['write'] ?>"/>
                        <?= (isset($set_forum['preview']) && $set_forum['preview'] ? '<input class="btn btn-info btn-sm" type="submit" value="' . $lng['preview'] . '" style="width: 107px; cursor: pointer;"/>' : '') ?>
                        <input type="hidden" name="token" value="<?= $token ?>"/>
                        </form>
                        <br>
                        <?
                    } else {
                        echo '<p><input class="btn btn-success btn-sm" type="submit" name="submit" value="' . $lng['write'] . '"/></p></form>';
                    }
                }

                echo '<p>' . $lng['total'] . ': ' . $colmes . '</p>';

                // Постраничная навигация
                if ($colmes > $kmess) {
                    echo functions::display_pagination('index.php?id=' . $id . '&amp;', $start, $colmes, $kmess);
                } else {
                    echo '<br />';
                }

                // Список кураторов
                if ($curators) {
                    $array = array();
                    foreach ($curators as $key => $value) {
                        $array[] = '<a href="../users/profile.php?user=' . $key . '">' . $value . '</a>';
                    }
                    echo '<p><div class="func">' . $lng_forum['curators'] . ': ' . implode(', ',
                            $array) . '</div></p>';
                }

                // Ссылки на модерские функции управления темой
                if ($rights == 3 || $rights >= 6) {
                    echo '<p><div class="func">';
                    if ($rights >= 7) {
                        echo '<a href="index.php?act=curators&amp;id=' . $id . '&amp;start=' . $start . '">' . $lng_forum['curators_of_the_topic'] . '</a><br />';
                    }
                    echo isset($topic_vote) && $topic_vote > 0
                        ? '<a href="index.php?act=editvote&amp;id=' . $id . '">' . $lng_forum['edit_vote'] . '</a><br/><a href="index.php?act=delvote&amp;id=' . $id . '">' . $lng_forum['delete_vote'] . '</a><br/>'
                        : '<a href="index.php?act=addvote&amp;id=' . $id . '">' . $lng_forum['add_vote'] . '</a><br/>';
                    echo '<a href="index.php?act=ren&amp;id=' . $id . '">' . $lng_forum['topic_rename'] . '</a><br/>';
                    // Закрыть - открыть тему
                    if ($type1['edit'] == 1) {
                        echo '<a href="index.php?act=close&amp;id=' . $id . '">' . $lng_forum['topic_open'] . '</a><br/>';
                    } else {
                        echo '<a href="index.php?act=close&amp;id=' . $id . '&amp;closed">' . $lng_forum['topic_close'] . '</a><br/>';
                    }
                    // Удалить - восстановить тему
                    if ($type1['close'] == 1) {
                        echo '<a href="index.php?act=restore&amp;id=' . $id . '">' . $lng_forum['topic_restore'] . '</a><br/>';
                    }
                    echo '<a href="index.php?act=deltema&amp;id=' . $id . '">' . $lng_forum['topic_delete'] . '</a><br/>';
                    if ($type1['vip'] == 1) {
                        echo '<a href="index.php?act=vip&amp;id=' . $id . '">' . $lng_forum['topic_unfix'] . '</a>';
                    } else {
                        echo '<a href="index.php?act=vip&amp;id=' . $id . '&amp;vip">' . $lng_forum['topic_fix'] . '</a>';
                    }
                    echo '<br/><a href="index.php?act=per&amp;id=' . $id . '">' . $lng_forum['topic_move'] . '</a></div></p>';
                }

                // Ссылка на список "Кто в теме"


                // Ссылка на фильтр постов
                if ($filter) {
                    echo '<div><a href="index.php?act=filter&amp;id=' . $id . '&amp;do=unset">' . $lng_forum['filter_cancel'] . '</a></div>';
                } else {
                    echo '<div><a href="index.php?act=filter&amp;id=' . $id . '&amp;start=' . $start . '">' . $lng_forum['filter_on_author'] . '</a></div>';
                }

                // Ссылка на скачку темы
                echo '<a href="index.php?act=tema&amp;id=' . $id . '">' . $lng_forum['download_topic'] . '</a>';
                break;

            default:
                // Если неверные данные, показываем ошибку
                echo functions::display_error($lng['error_wrong_data']);
                break;
        }
    } else {
        ////////////////////////////////////////////////////////////
        // Список Категорий форума                                //
        ////////////////////////////////////////////////////////////
        $count = DB::getInstance()->getCount(DB::getInstance()->query("SELECT COUNT(*) FROM `cms_forum_files`" . ($rights >= 7 ? '' : " WHERE `del` != '1'")));
            ?>
            <ul class="nav nav-pills">
                <li><?= counters::forum_new(1) ?></li>
                <li><a href="search.php"><?= $lng['search'] ?></a></li>
                <li><a href="index.php?act=files"><?= $lng_forum['files_forum'] ?> <span class="badge"><?= $count ?></span></a></li>
            </ul>

<?

        $req = DB::getInstance()->query("SELECT `id`, `text`, `soft` FROM `forum` WHERE `type`='f' ORDER BY `realid`");
        $i = 0;
        while ($res = DB::getInstance()->getAssoc($req)) {
            echo $i % 2 ? '<div class="list2 forum_post">' : '<div class="list1 forum_post">';
            $count = DB::getInstance()->getCount(DB::getInstance()->query("SELECT COUNT(*) FROM `forum` WHERE `type`='r' AND `refid`='" . $res['id'] . "'"));
            echo '<a href="index.php?id=' . $res['id'] . '">' . $res['text'] . '</a> [' . $count . ']';
            if (!empty($res['soft'])) {
                echo '<div class="sub browser"><span class="gray">' . $res['soft'] . '</span></div>';
            }
            echo '</div>';
            ++$i;
        }
        $online_u = DB::getInstance()->getCount(DB::getInstance()->query("SELECT COUNT(*) FROM `users` WHERE `lastdate` > " . (time() - 300) . " AND `place` LIKE 'forum%'"));
        $online_g = DB::getInstance()->getCount(DB::getInstance()->query("SELECT COUNT(*) FROM `cms_sessions` WHERE `lastdate` > " . (time() - 300) . " AND `place` LIKE 'forum%'"));
        echo '<div class="phdr">' . ($user_id ? '<a href="index.php?act=who">' . $lng_forum['who_in_forum'] . '</a>' : $lng_forum['who_in_forum']) . '&#160;(' . $online_u . '&#160;/&#160;' . $online_g . ')</div>';
        unset($_SESSION['fsort_id']);
        unset($_SESSION['fsort_users']);
    }

    // Навигация внизу страницы
    echo '<p>' . ($id ? '<a href="index.php">' . $lng['to_forum'] . '</a><br />' : '');
    if (!$id) {
        echo '<a href="../pages/faq.php?act=forum">' . $lng_forum['forum_rules'] . '</a>';
    }
    echo '</p>';
    if (!$user_id) {
        if ((empty($_SESSION['uppost'])) || ($_SESSION['uppost'] == 0)) {
            echo '<a href="index.php?id=' . $id . '&amp;page=' . $page . '&amp;newup">' . $lng_forum['new_on_top'] . '</a>';
        } else {
            echo '<a href="index.php?id=' . $id . '&amp;page=' . $page . '&amp;newdown">' . $lng_forum['new_on_bottom'] . '</a>';
        }
    }
}

require_once('../incfiles/end.php');