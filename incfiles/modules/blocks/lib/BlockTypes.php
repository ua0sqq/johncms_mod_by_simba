<?php
/**
 * Created by PhpStorm.
 * User: Maxim Masalov
 * Date: 22.06.2016
 * Time: 17:33
 * Project: johncms_mod_by_simba
 */
namespace System\Blocks;

use System\Core\DB as DB;

class BlockTypes
{
    public $last_errors = [];

    public $last_count = 0;

    public function __construct()
    {
    }

    /**
     * Метод добавляет тип инфоблоков
     * @param array $fields - Массив полей таблицы
     * @return bool
     */
    public function add($fields = [])
    {
        //TODO: добавить систему событий
        $errors = [];

        if (empty($fields['name'])) {
            $errors[] = 'Не указано название типа инфоблоков';
        }

        if (empty($fields['code']) || !preg_match('#^[a-zA-Z0-9_]+$#', $fields['code'])) {
            $errors[] = 'Код типа инфоблока должен состоять только из букв латинского алфавита, цифр и знака нижнего подчёркивания';
        }

        if (empty($errors)) {

            DB::getInstance()->insert('s_block_types', $fields);
            return true;
        }

        $this->last_errors = $errors;
        return false;
    }


    /**
     * Метод удаляет тип инфоблоков с указанным ID
     * @param $id
     * @return bool
     */
    public function delete($id)
    {
        $res_count = DB::getInstance()->query("SELECT `id` FROM `s_blocks` WHERE `type` = '" . intval($id) . "';");
        if (DB::getInstance()->getCount($res_count) > 0) {
            $this->last_errors[] = 'У данного типа есть привязанные инфоблоки. Сначала удалите их!';
            return false;
        }

        DB::getInstance()->query('DELETE FROM `s_block_types` WHERE `id` = ' . $id . ';');
        return true;
    }


    /**
     * Метод получает список типов инфоблоков
     * @param string $filter
     * @param array $limit
     * @param array $order
     * @return bool|resource
     */
    public function getList($filter = '', $limit = [], $order = [])
    {
        global $DB;
        //$filter = $filter;

        $res = $DB->query('SELECT COUNT(*) FROM `s_block_types`;');
        $this->last_count = $DB->getCount($res);

        return DB::getInstance()->getList('s_block_types', [], $filter, $limit, $order);
    }


    /**
     * Метод обновляет тип инфоблоков
     * @param $id - ID типа
     * @param $fields - Массив полей, где ключ - название поля, значение - значение.
     * @return bool
     */
    public function update($id, $fields)
    {
        if (empty($fields['name'])) {
            $errors[] = 'Не указано название типа инфоблоков';
        }

        if (!empty($fields['code']) && !preg_match('#^[a-zA-Z0-9_]+$#', $fields['code'])) {
            $errors[] = 'Код типа инфоблока должен состоять только из букв латинского алфавита, цифр и знака нижнего подчёркивания';
        }

        if(empty($errors)) {
            DB::getInstance()->update('s_block_types', $fields, 'WHERE `id` = ' . intval($id) . '');
            return true;
        }

        return false;
    }


    /**
     * Получает тип инфоблоков по ID
     * @param $id
     * @return array
     */
    public static function getByID($id)
    {
        $res = DB::getInstance()->query('SELECT * FROM `s_block_types` WHERE `id` = ' . $id . ';');
        $arr = DB::getInstance()->getAssoc($res);
        return $arr;
    }

}