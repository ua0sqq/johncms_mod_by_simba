<?php
/**
 * Created by PhpStorm.
 * User: Maxim Masalov
 * Date: 27.06.2016
 * Time: 18:42
 * Project: johncms_mod_by_simba
 */

namespace System\Blocks;

use System\Core\DB as DB;

class IBlocks
{

    /**
     * Содержит ошибки полученные при выполнении различных действий
     * @var array
     */
    public $last_errors = [];

    public $count_result = 0;

    /**
     * Метод для создания инфоблока
     *
     * @param array $fields - Массив полей.
     * @return bool
     */
    public function add($fields = [])
    {
        $errors = [];
        if (empty($fields['name'])) {
            $errors[] = 'Не указано название инфоблока';
        }

        if (empty($fields['code']) || !preg_match('#^[a-zA-Z0-9_]+$#', $fields['code'])) {
            $errors[] = 'Код инфоблока должен состоять только из букв латинского алфавита, цифр и знака нижнего подчёркивания';
        }

        if (empty($fields['table_name']) || !preg_match('#^[a-zA-Z0-9_]+$#', $fields['table_name'])) {
            $errors[] = 'Название таблицы инфоблока должно состоять только из букв латинского алфавита, цифр и знака нижнего подчёркивания';
        }

        if (empty($fields['type'])) {
            $errors[] = 'Не указан тип инфоблока';
        }

        $link = DB::getInstance()->query("SHOW TABLES LIKE 's_b_" . DB::getInstance()->toSql($fields['table_name']) . "'");

        if (DB::getInstance()->numRows($link) > 0) {
            $errors[] = 'Таблица с указанным именем уже существует';
        }

        // Если нет ошибок, создаём таблицу
        if (empty($errors)) {

            // Создаем таблицу
            DB::getInstance()->query("CREATE TABLE IF NOT EXISTS `s_b_" . DB::getInstance()->toSql($fields['table_name']) . "` (
            `id` INT(11) NOT NULL, 
            `date_created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
            `date_updated` DATETIME DEFAULT NULL) 
            ENGINE=InnoDB DEFAULT CHARSET=utf8;");

            // Проставляем индексы
            DB::getInstance()->query("ALTER TABLE `s_b_" . DB::getInstance()->toSql($fields['table_name']) . "` ADD PRIMARY KEY (`id`);");
            DB::getInstance()->query("ALTER TABLE `s_b_" . DB::getInstance()->toSql($fields['table_name']) . "` MODIFY `id` INT(11) NOT NULL AUTO_INCREMENT;");

            // Сохраняем информацию в таблицу с инфолоками
            DB::getInstance()->insert('s_blocks', $fields);
            return true;
        }

        $this->last_errors = $errors;
        return false;
    }


    /**
     * Удаление инфоблока по ID
     * @param int $id
     * @return bool
     */
    public function delete($id)
    {
        $arr_iblock = self::getByID($id);
        if (!empty($arr_iblock)) {

            // Удаление таблицы из БД
            DB::getInstance()->query('DROP TABLE `s_b_' . $arr_iblock['table_name'] . '`');

            // Удаляем запись из таблицы инфоблоков
            DB::getInstance()->query('DELETE FROM `s_blocks` WHERE `id` = ' . intval($id));

            //TODO: Добавить удаление свойств и прочих связанных данных по мере готовности функционала

            return true;
        }

        return false;
    }


    /**
     * Метод для получения инфоблока по ID
     *
     * @param int $id - ID инфоблока
     * @return array
     */
    public function getByID($id)
    {
        $res = DB::getInstance()->query('SELECT * FROM `s_blocks` WHERE `id` = ' . intval($id));
        if ($arr = DB::getInstance()->getAssoc($res)) {
            return $arr;
        }
        return [];
    }


    /**
     * Метод получает список инфоблоков
     *
     * @param array $filter
     * @param array $limit
     * @param array $order
     * @return array|bool|resource
     */
    public function getList($filter = [], $limit = [], $order = [])
    {
        global $DB;
        $where = '';
        if (!empty($filter) && is_array($filter)) {
            foreach ($filter as $item) {
                if (!empty($where)) {
                    $where .= ' AND ';
                }
                $where .= '`' . $DB->toSql($item[0]) . '` ' . $DB->toSql($item[1]) . ' ' . $DB->toSql($item[2]) . '';
            }
        }

        $res_count = $DB->query('SELECT COUNT(*) FROM `s_blocks` ' . (!empty($where) ? ' WHERE ' . $where : ''));
        $this->count_result = $DB->getCount($res_count);

        return $DB->getList('s_blocks', [], $where, $limit, $order);
    }


    /**
     * Метод обновляет поля инфоблока
     * @param $id - ID инфоблока
     * @param array $fields - Массив полей и значений
     * @return bool
     */
    public function update($id, $fields = [])
    {
        global $DB;

        $errors = [];
        if (empty($fields['name'])) {
            $errors[] = 'Не указано название инфоблока';
        }

        if (empty($fields['code']) || !preg_match('#^[a-zA-Z0-9_]+$#', $fields['code'])) {
            $errors[] = 'Код инфоблока должен состоять только из букв латинского алфавита, цифр и знака нижнего подчёркивания';
        }

        // Обновляем поля инфоблока
        if (empty($errors)) {
            $DB->update('s_blocks', [
                'name' => $fields['name'],
                'code' => $fields['code'],
            ], 'WHERE `id` = ' . intval($id));
            return true;
        }

        $this->last_errors = $errors;
        return false;
    }


}