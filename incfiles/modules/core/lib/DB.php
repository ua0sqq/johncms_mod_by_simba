<?php
/**
 * @author Maxim Masalov
 * @file DB.php
 * @created 04.03.2015 16:58
 */

namespace System\Core;

use System\Core\Config as Config;

/**
 * Class for using database
 * Class DB
 */
class DB
{

    /**
     * @var object - Singleton
     */
    protected static $_instance;

    /**
     * Query counter
     * @var int
     */
    public $query_count = 0;

    /**
     * Errors
     * @var array
     */
    public $errors = array();


    private $connect;

    /**
     * Class constructor
     */
    private function __construct()
    {
        $db_config = Config::getInstance()->load('db');

        $db_host = isset($db_config['HOST']) ? $db_config['HOST'] : 'localhost';
        $db_user = isset($db_config['USER']) ? $db_config['USER'] : '';
        $db_pass = isset($db_config['PASSWORD']) ? $db_config['PASSWORD'] : '';
        $db_name = isset($db_config['DATABASE']) ? $db_config['DATABASE'] : '';

        $this->connect = new \mysqli($db_host, $db_user, $db_pass, $db_name);
        if ($this->connect->connect_errno) {
            echo "Error: cannot connect to database server: " . $this->connect->connect_error;
        }

        $this->query("SET NAMES 'utf8'");
    }


    /**
     * Close access to functions outside of the class
     */
    private function __clone()
    {
    }


    /**
     * This is singleton pattern
     *
     * @return DB
     */
    public static function getInstance()
    {
        // Check actuality instance
        if (null === self::$_instance) {
            // Create new instance
            self::$_instance = new self();
        }

        // return instance
        return self::$_instance;
    }


    /**
     * This method send query to database
     *
     * @param $sql - Query string
     * @return resource - result
     */
    public function query($sql)
    {
        $this->query_count++;
        return $this->connect->query($sql);
    }


    /**
     * This method send multi-query
     *
     * @param $sql
     * @return bool
     */
    public function multiQuery($sql)
    {
        $this->query_count++;
        return $this->connect->multi_query($sql);
    }


    /**
     * @return \mysqli_result
     */
    public function storeResult()
    {
        return $this->connect->store_result();
    }


    /**
     * @return bool
     */
    public function nextResult()
    {
        return $this->connect->next_result();
    }


    /**
     * @return bool
     */
    public function moreResult()
    {
        return $this->connect->more_results();
    }

    /**
     * This method insert data to database
     *
     * @param $table - Table to insert
     * @param $fields - Fields to insert
     * @return bool|string
     */
    public function insert($table, $fields)
    {

        if (empty($table)) {
            return false;
        }

        if (!empty($fields) AND is_array($fields)) {
            $arSet = array();
            foreach ($fields as $field => $value) {
                if (strlen($value) <= 0) {
                    $arSet[] = "`" . $field . "` = ''";
                } else {
                    $arSet[] = "`" . $field . "` = '" . $this->toSql($value) . "'";
                }
            }

            $sql = 'INSERT INTO `' . $table . '` SET ' . implode(', ', $arSet);

            $return = $this->query($sql);

            return $return;
        }

        return false;
    }


    /**
     * Return last inserted ID
     * @return int
     */
    public function lastID()
    {
        return $this->connect->insert_id;
    }

    /**
     * This method update the string in database
     *
     * @param $table - Table to insert
     * @param $fields - Fields to insert
     * @param $where - Where string
     * @return bool|string
     */
    public function update($table, $fields, $where = '')
    {

        if (empty($table)) {
            return false;
        }

        if (!empty($fields) AND is_array($fields)) {
            $arSet = array();
            foreach ($fields as $field => $value) {
                if (strlen($value) <= 0) {
                    $arSet[] = "`" . $field . "` = ''";
                } else {
                    $arSet[] = "`" . $field . "` = '" . $this->toSql($value) . "'";
                }
            }

            $sql = 'UPDATE `' . $table . '` SET ' . implode(', ', $arSet);

            if (!empty($where)) {
                $sql .= ' ' . $where;
            }

            $return = $this->query($sql);

            return $return;
        }

        return false;
    }


    /**
     * This method executes query to db
     *
     * @param $table - Table name
     * @param array $fields - array fields for select
     * @param string $where - the conditions selection
     * @param array $limit - Limit
     * @param array $order - Order by
     * @return bool|resource|array
     */
    public function getList($table, $fields = array(), $where = '', $limit = [], $order = [])
    {

        if (empty($table)) {
            return false;
        }
        $sql = 'SELECT ' . ((!empty($fields) AND is_array($fields)) ? implode(', ', $fields) : '*') . ' FROM ' . $table;

        if (!empty($where)) {
            $sql = $sql . ' WHERE ' . $where;
        }

        if (!empty($order) && is_array($order)) {
            $sql .= ' ORDER BY ';
            $orders = '';
            foreach ($order as $key => $val) {
                if (!empty($orders)) {
                    $orders .= ', ';
                }
                $orders .= "`" . $this->toSql($key) . "` " . $this->toSql($val) . "";
            }

            $sql .= $orders;
        }

        if (!empty($limit) && is_array($limit)) {
            $sql = $sql . ' LIMIT ' . intval(key($limit)) . ', ' . intval($limit[key($limit)]);
        }

        $return = [];
        $res = $this->query($sql);
        while ($arr = $this->getAssoc($res)) {
            $return[] = $arr;
        }

        return $return;
    }


    /**
     * This method gets array
     *
     * @param resource $link
     * @return array
     */
    public function getAssoc($link)
    {
        return $link->fetch_assoc();
    }


    /**
     * This method returns count elements for query SELECT COUNT()
     *
     * @param $link
     * @param $row
     * @return string
     */
    public function getCount($link, $row = 0)
    {
        $arr = $link->fetch_array();
        return $arr[$row];
    }


    /**
     * Prepare string for sql query
     *
     * @param string $str
     * @return string
     */
    public function toSql($str = '')
    {
        return $this->connect->real_escape_string($str);
    }


    /**
     * Get number rows
     *
     * @param $link
     * @return mixed
     */
    public function numRows($link)
    {
        return $link->num_rows;
    }


    /**
     * Get error
     *
     * @return string
     */
    public function error()
    {
        return $this->connect->error;
    }


    /**
     * affected rows
     *
     * @return int
     */
    public function affectedRows()
    {
        return $this->connect->affected_rows;
    }

}