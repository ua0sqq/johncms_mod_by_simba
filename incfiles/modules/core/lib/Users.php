<?php
/**
 * Created by PhpStorm.
 * User: Maxim Masalov
 * Date: 09.08.2016
 * Time: 17:35
 * Project: johncms_mod_by_simba
 */

namespace System\Core;


class Users
{

    /**
     * Получаем список пользователей
     *
     * @param array $sort - Sort params
     * @param array $filter - Filter users
     * @param array $limit - Limit
     * @return array
     */
    public static function getList($sort = [], $filter = [], $limit = [])
    {
        $where = '';
        if (!empty($filter) && is_array($filter)) {
            foreach ($filter as $key => $val) {
                if (!empty($where)) {
                    $where .= ' AND ';
                }

                $field = DB::getInstance()->toSql($val[0]);
                $cond = DB::getInstance()->toSql($val[1]);
                $value = DB::getInstance()->toSql($val[2]);

                $where .= "`" . $field . "` " . $cond . " '" . $value . "'";
            }
        }

        return DB::getInstance()->getList('users', [], $where, $limit, $sort);
    }


    /**
     * Счётчик юзеров
     *
     * @param array $filter
     * @return string
     */
    public static function countUsers($filter = [])
    {

        $where = '';
        if (!empty($filter) && is_array($filter)) {
            foreach ($filter as $key => $val) {
                if (!empty($where)) {
                    $where .= ' AND ';
                }

                $field = DB::getInstance()->toSql($val[0]);
                $cond = DB::getInstance()->toSql($val[1]);
                $value = DB::getInstance()->toSql($val[2]);

                $where .= "`" . $field . "` " . $cond . " '" . $value . "'";
            }
        }

        $where = (!empty($where) ? ' WHERE ' . $where : '');
        $res = DB::getInstance()->query("SELECT COUNT(*) FROM `users` $where");

        return DB::getInstance()->getCount($res);
    }


    /**
     * Проверка авторизован ли пользователь или нет
     *
     * @return bool
     */
    public static function isAuthorized()
    {

        if (\core::$user_id > 0) {
            return true;
        }

        return false;
    }


    /**
     * Авторизация пользователя
     *
     * @param $login
     * @param $password
     * @param bool $save
     * @return bool
     */
    public static function authorize($login, $password, $save = true)
    {
        $login = DB::getInstance()->toSql($login);

        $req = DB::getInstance()->query("SELECT * FROM `users` WHERE `name_lat`='" . \functions::rus_lat(mb_strtolower($login)) . "' LIMIT 1");
        if (DB::getInstance()->numRows($req)) {
            $user = DB::getInstance()->getAssoc($req);
            if (md5(md5($password)) == $user['password']) {

                DB::getInstance()->query("UPDATE `users` SET `failed_login` = '0', `sestime` = '" . time() . "' WHERE `id` = '" . $user['id'] . "'");

                // Если все проверки прошли удачно, подготавливаем вход на сайт
                if ($save === true) {
                    // Установка данных COOKIE
                    $cuid = base64_encode($user['id']);
                    $cups = md5($password);
                    setcookie("cuid", $cuid, time() + 3600 * 24 * 365, '/');
                    setcookie("cups", $cups, time() + 3600 * 24 * 365, '/');
                }

                // Установка данных сессии
                $_SESSION['uid'] = $user['id'];
                $_SESSION['ups'] = md5(md5($password));

                return true;
            }

        }

        return false;
    }


    /**
     * Проверяем есть ли у текучего пользователя доступ в админку
     *
     * @return bool
     */
    public static function adminAccess()
    {
        if(\core::$user_rights >= 7) {
            return true;
        }
        return false;
    }


    /**
     * Завершает сессию и удаляет авторизацию по кукам
     *
     * @return bool
     */
    public static function logout()
    {
        setcookie('cuid', '', null, '/');
        setcookie('cups', '', null, '/');
        session_destroy();

        return true;
    }

}