<?php defined('_IN_JOHNCMS') or die ('Error: restricted access');

return [
    [
        'exp'  => '#^/admin/iblocks/add_element/#',
        'file' => '/admin/modules/iblocks/add_element.php'
    ],
    [
        'exp'  => '#^/admin/iblocks/del_element/#',
        'file' => '/admin/modules/iblocks/del_element.php'
    ],
    [
        'exp'  => '#^/admin/iblocks/edit_element/#',
        'file' => '/admin/modules/iblocks/edit_element.php'
    ],
    [
        'exp'  => '#^/admin/iblocks/#',
        'file' => '/admin/modules/iblocks/index.php'
    ],
    [
        'exp'  => '#^/admin/settings/iblocks/add/#',
        'file' => '/admin/modules/iblocks/add_type.php'
    ],
    [
        'exp'  => '#^/admin/settings/iblocks/props/#',
        'file' => '/admin/modules/iblocks/iblock_props.php'
    ],
    [
        'exp'  => '#^/admin/settings/iblocks/add_prop/#',
        'file' => '/admin/modules/iblocks/iblock_add_prop.php'
    ],
    [
        'exp'  => '#^/admin/settings/iblocks/delete_prop/#',
        'file' => '/admin/modules/iblocks/iblock_del_prop.php'
    ],
    [
        'exp'  => '#^/admin/settings/iblocks/add_iblock/#',
        'file' => '/admin/modules/iblocks/add_iblock.php'
    ],
    [
        'exp'  => '#^/admin/settings/iblocks/delete/#',
        'file' => '/admin/modules/iblocks/delete.php'
    ],
    [
        'exp'  => '#^/admin/settings/iblocks/list/#',
        'file' => '/admin/modules/iblocks/iblocks.php'
    ],
    [
        'exp'  => '#^/admin/settings/iblocks/edit/#',
        'file' => '/admin/modules/iblocks/edit_iblock.php'
    ],
    [
        'exp'  => '#^/admin/settings/iblocks/#',
        'file' => '/admin/modules/iblocks/types.php'
    ]
];





