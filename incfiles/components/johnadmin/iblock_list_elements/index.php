<?php defined('_IN_JOHNCMS') or die('Restricted access');
/**
 * @var $this Component
 */

// $this->params - Содержит параметры компонента (массив)
$result = [];
$list = [];


if (!empty($this->params['iblock_id'])) {
    $ibElements = new System\Blocks\Elements($this->params['iblock_id']);
    PageBuffer::getInstance()->setTitle($ibElements->iblock_data['name']);


    // Если ajax запрос, собираем массив для datatables
    if (isset($_REQUEST['is_ajax'])) {
        // Очищаем буфер вывода
        \PageBuffer::getInstance()->cleanBuffer();

        // Сдвиги выборки для постраничной навигации
        $offset = (!empty($_REQUEST['start']) ? intval($_REQUEST['start']) : 0);
        $limit = (!empty($_REQUEST['length']) ? intval($_REQUEST['length']) : 0);

        // Получаем список элементов инфоблока
        $list = $ibElements->getList([], $offset, $limit);

        foreach ($list as $key => $item) {
            $list[$key] = array_map('htmlspecialchars', $item);
            $list[$key]['actions'] = '
            <a href="/admin/iblocks/del_element/?iblock_id='.$this->params['iblock_id'].'&id='.$item['id'].'">Удалить</a><br>
            <a href="/admin/iblocks/edit_element/?iblock_id='.$this->params['iblock_id'].'&id='.$item['id'].'">Изменить</a>
            ';
        }

        // Собираем массив данных понятный для jquery datatables
        $draw = isset($_REQUEST['draw']) ? intval($_REQUEST['draw']) : 1;
        $return_array = [
            "draw"            => $draw,
            "recordsTotal"    => $ibElements->count_result,
            "recordsFiltered" => $ibElements->count_result,
            "data"            => $list
        ];

        echo json_encode($return_array);

        exit();
    }


    $result['columns'] = $ibElements->getColumns();
    $result['columns']['actions'] = 'Действия';
}


$result['items'] = $list;

// Массив который попадает в шаблон
$this->result = $result;

// Загружаем языки
$this->loadLanguage();

// Загружаем шаблон
$this->loadTemplate();
