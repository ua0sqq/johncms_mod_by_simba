<?php defined('_IN_JOHNCMS') or die('Error: restricted access');
/**
 * @var $this Component
 */

// p($this->result);

?>

<? if (!empty($this->result)): ?>
    <div class="row tile_count">
        <? foreach ($this->result as $item): ?>
            <div class="col-md-3 col-sm-5 col-xs-6 tile_stats_count">
                <span class="count_top">
                    <i class="fa <?= $item['icon'] ?>"></i> <?= $item['name'] ?>
                </span>
                <div class="count"><?= $item['total'] ?></div>
                <span class="count_bottom">
                    <?= ($item['last'] > 0 ? '<i class="green">+' . $item['last'] . '</i>' : 0) ?>
                    <?= $item['last_suffix'] ?>
                </span>
            </div>
        <? endforeach; ?>
    </div>
<? endif; ?>
