<?php defined('_IN_JOHNCMS') or die('Error: restricted access');
/**
 * @var $this Component
 */
?>

<? core::loadComponent('johnadmin', 'form', 'default', [
    'fields' => $this->result['fields'],
    'errors' => $this->result['errors'],
    'action' => '/admin/iblocks/edit_element/?iblock_id=' . $this->params['iblock'] . '&id=' . $this->params['id'],
    'cancel' => '/admin/iblocks/index.php?id=' . $this->params['iblock']
]); ?>