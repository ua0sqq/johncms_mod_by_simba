<?php defined('_IN_JOHNCMS') or die('Error: restricted access');
/**
 * @var $this Component
 */
?>

<? core::loadComponent('johnadmin', 'form', 'default', [
    'fields' => $this->result['fields'],
    'errors' => $this->result['errors'],
    'action' => '/admin/iblocks/add_element/?iblock='.$this->result['iblock'],
    'cancel' => '/admin/iblocks/index.php?id='.$this->result['iblock']
]); ?>