<?php defined('_IN_JOHNCMS') or die('Error: restricted access');
/**
 * @var $this Component
 */
?>

<? core::loadComponent('johnadmin', 'form', 'default', [
    'fields' => $this->result['fields'],
    'errors' => $this->result['errors'],
    'action' => '/admin/settings/iblocks/add/',
    'cancel' => '/admin/settings/iblocks/'
]); ?>