<?php defined('_IN_JOHNCMS') or die('Restricted access');
/**
 * @var $this Component
 */

// $this->params - Содержит параметры компонента (массив)
$result = [];
$list = [];
$result['count_elements'] = 0;

$this->params['nav_length'] = isset($this->params['nav_length']) ? intval($this->params['nav_length']) : 10;

if (!empty($this->params['iblock_id'])) {
    $ibElements = new System\Blocks\Elements($this->params['iblock_id']);

    // Сдвиги выборки для постраничной навигации

    $limit = (!empty($_REQUEST['length']) ? intval($_REQUEST['length']) : $this->params['nav_length']);
    $page = isset($_REQUEST['page']) && $_REQUEST['page'] > 0 ? intval($_REQUEST['page']) : 1;
    $offset = isset($_REQUEST['page']) ? $page * $limit - $limit : (isset($_GET['start']) ? abs(intval($_GET['start'])) : 0);

    // Получаем список элементов инфоблока
    $list = $ibElements->getList([], $offset, $limit);

    if ($ibElements->count_result > $limit) {
        $result['pagination'] = \functions::display_pagination('?', $offset, $ibElements->count_result, $limit);
    } else {
        $result['pagination'] = '';
    }

    $result['count_elements'] = $ibElements->count_result;
}


$result['items'] = $list;

// Массив который попадает в шаблон
$this->result = $result;

// Загружаем языки
$this->loadLanguage();

// Загружаем шаблон
$this->loadTemplate();
