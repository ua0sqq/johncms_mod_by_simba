<?php define('_IN_JOHNCMS', 1);

/**
 * Created by PhpStorm.
 * User: simba
 * Date: 09.02.2016
 * Time: 19:40
 */


require_once $_SERVER['DOCUMENT_ROOT'] . '/incfiles/core.php';
$routeRules = System\Core\Config::getInstance()->load('rule_route');
$requestUri = $_SERVER['REQUEST_URI'];

if (!empty($routeRules)) {
    foreach ($routeRules as $rule) {
        if (preg_match($rule['exp'], $requestUri)) {
            if (!empty($rule['file']) && file_exists($_SERVER['DOCUMENT_ROOT'] . $rule['file'])) {
                PageBuffer::getInstance()->addHeader('HTTP/1.1 200 Ok');
                require_once $_SERVER['DOCUMENT_ROOT'] . $rule['file'];
                die();
            }
        }
    }
}

require $_SERVER['DOCUMENT_ROOT'].'/404.php';
die();

