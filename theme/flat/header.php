<?php defined('_IN_JOHNCMS') or die('Restricted access');
/**
 * Created by PhpStorm.
 * User: simba
 * Date: 26.01.2016
 * Time: 19:43
 */


$headmod = isset($headmod) ? $DB->toSql($headmod) : '';
$textl = isset($textl) ? $textl : core::$system_set['copyright'];

// Устанавливаем заголовок страницы, ключевые слова и описание
PageBuffer::getInstance()->setTitle($textl);
PageBuffer::getInstance()->addMeta('keywords', $set['meta_key']);
PageBuffer::getInstance()->addMeta('description', $set['meta_desc']);

// Добавляем JS
PageBuffer::getInstance()->addJs('/theme/' . $set_user['skin'] . '/js/jquery.min.js');
PageBuffer::getInstance()->addJs('/theme/' . $set_user['skin'] . '/js/bootstrap.min.js');
PageBuffer::getInstance()->addJs('/theme/' . $set_user['skin'] . '/wysibb/jquery.wysibb.min.js');
PageBuffer::getInstance()->addJs('/theme/' . $set_user['skin'] . '/wysibb/lang/ru.min.js');
PageBuffer::getInstance()->addJs('/theme/' . $set_user['skin'] . '/js/johncms.js');


// Добавляем CSS
PageBuffer::getInstance()->addCss('/theme/' . $set_user['skin'] . '/css/bootstrap.min.css');
PageBuffer::getInstance()->addCss('/theme/' . $set_user['skin'] . '/css/font-awesome.min.css');
PageBuffer::getInstance()->addCss('/theme/' . $set_user['skin'] . '/css/animate.css');
PageBuffer::getInstance()->addCss('/theme/' . $set_user['skin'] . '/css/main.css');
PageBuffer::getInstance()->addCss('/theme/' . $set_user['skin'] . '/css/custom.css');
PageBuffer::getInstance()->addCss('/theme/' . $set_user['skin'] . '/wysibb/theme/default/wbbtheme.min.css');


?><!DOCTYPE html>
<html lang="<?= core::$lng_iso ?>">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <? PageBuffer::getInstance()->showMeta() ?>
        <link rel="shortcut icon" href="<?= $set['homeurl'] ?>/favicon.ico">
        <title><? PageBuffer::getInstance()->showTitle(); ?></title>
        <? PageBuffer::getInstance()->showCss() ?>

        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
<body>
<?= core::display_core_errors() ?>

    <header class="navbar navbar-inverse wet-asphalt" role="banner">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/">JohnCMS</a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">

                    <? core::loadComponent('johncms', 'menu', 'default', array('menu_type' => 'top')); ?>

                    <? if ($user_id): ?>

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-expanded="false"><?= $login ?> <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="/users/profile.php?act=office"><?= $lng['personal'] ?></a></li>
                                <li class="divider"></li>
                                <li><a href="/exit.php"><?= $lng['exit'] ?></a></li>
                            </ul>
                        </li>
                    <? else: ?>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-expanded="false"><?= $lng['login'] ?> <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="/login.php"><?= $lng['login'] ?></a></li>
                                <li><a href="/registration.php"><?= $lng['registration'] ?></a></li>
                            </ul>
                        </li>
                    <? endif; ?>
                </ul>
            </div>
        </div>
    </header><!--/header-->
    <? if($headmod != 'mainpage'): ?>
    <section id="title" class="emerald">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <h1><? PageBuffer::getInstance()->showTitle(true); ?></h1>
                </div>
                <div class="col-sm-6">
                    <? PageBuffer::getInstance()->showBreadCrumbs(); ?>
                </div>
            </div>
        </div>
    </section><!--/#title-->

    <div class="container">
    <? endif; ?>
<?
/*
-----------------------------------------------------------------
Рекламный модуль
-----------------------------------------------------------------
*/
$cms_ads = array();
if (!isset($_GET['err']) && $act != '404' && $headmod != 'admin') {
    $view = $user_id ? 2 : 1;
    $layout = ($headmod == 'mainpage' && !$act) ? 1 : 2;
    $req = $DB->query("SELECT * FROM `cms_ads` WHERE `to` = '0' AND (`layout` = '$layout' or `layout` = '0') AND (`view` = '$view' or `view` = '0') ORDER BY  `mesto` ASC");
    if ($DB->numRows($req)) {
        while ($res = $DB->getAssoc($req)) {
            $name = explode("|", $res['name']);
            $name = htmlentities($name[mt_rand(0, (count($name) - 1))], ENT_QUOTES, 'UTF-8');
            if (!empty($res['color'])) {
                $name = '<span style="color:#' . $res['color'] . '">' . $name . '</span>';
            }
            // Если было задано начертание шрифта, то применяем
            $font = $res['bold'] ? 'font-weight: bold;' : false;
            $font .= $res['italic'] ? ' font-style:italic;' : false;
            $font .= $res['underline'] ? ' text-decoration:underline;' : false;
            if ($font) {
                $name = '<span style="' . $font . '">' . $name . '</span>';
            }
            @$cms_ads[$res['type']] .= '<a href="' . ($res['show'] ? functions::checkout($res['link']) : $set['homeurl'] . '/go.php?id=' . $res['id']) . '">' . $name . '</a><br/>';
            if (($res['day'] != 0 && time() >= ($res['time'] + $res['day'] * 3600 * 24)) || ($res['count_link'] != 0 && $res['count'] >= $res['count_link'])) {
                $DB->query("UPDATE `cms_ads` SET `to` = '1'  WHERE `id` = '" . $res['id'] . "'");
            }
        }
    }
}

/*
-----------------------------------------------------------------
Рекламный блок сайта
-----------------------------------------------------------------
*/
if (isset($cms_ads[0])) {
    echo $cms_ads[0];
}

/*
-----------------------------------------------------------------
Выводим логотип и переключатель языков
-----------------------------------------------------------------
*/
/*echo '<table style="width: 100%;" class="logo"><tr>' .
    '<td valign="bottom"><a href="' . $set['homeurl'] . '">' . functions::image('logo.gif', array('class' => '')) . '</a></td>' .
    ($headmod == 'mainpage' && count(core::$lng_list) > 1 ? '<td align="right"><a href="' . $set['homeurl'] . '/go.php?lng"><b>' . strtoupper(core::$lng_iso) . '</b></a>&#160;<img src="' . $set['homeurl'] . '/images/flags/' . core::$lng_iso . '.gif" alt=""/>&#160;</td>' : '') .
    '</tr></table>';*/

/*
-----------------------------------------------------------------
Выводим верхний блок с приветствием
-----------------------------------------------------------------
*/
/*echo '<div class="header"> ' . $lng['hi'] . ', ' . ($user_id ? '<b>' . $login . '</b>!' : $lng['guest'] . '!') . '</div>';*/

/*
-----------------------------------------------------------------
Главное меню пользователя
-----------------------------------------------------------------
*/
/*echo '<div class="tmn">' .
    (isset($_GET['err']) || $headmod != "mainpage" || ($headmod == 'mainpage' && $act) ? '<a href=\'' . $set['homeurl'] . '\'>' . functions::image('menu_home.png') . $lng['homepage'] . '</a><br/>' : '') .
    ($user_id && $headmod != 'office' ? '<a href="' . $set['homeurl'] . '/users/profile.php?act=office">' . functions::image('menu_cabinet.png') . $lng['personal'] . '</a><br/>' : '') .
    (!$user_id && $headmod != 'login' ? functions::image('menu_login.png') . '<a href="' . $set['homeurl'] . '/login.php">' . $lng['login'] . '</a>' : '') .
    '</div><div class="maintxt">';*/

/*
-----------------------------------------------------------------
Рекламный блок сайта
-----------------------------------------------------------------
*/
if (!empty($cms_ads[1])) {
    echo '<div class="gmenu">' . $cms_ads[1] . '</div>';
}

/*
-----------------------------------------------------------------
Фиксация местоположений посетителей
-----------------------------------------------------------------
*/
$sql = '';
$set_karma = unserialize($set['karma']);
if ($user_id) {
    // Фиксируем местоположение авторизованных
    if (!$datauser['karma_off'] && $set_karma['on'] && $datauser['karma_time'] <= (time() - 86400)) {
        $sql .= " `karma_time` = '" . time() . "', ";
    }
    $movings = $datauser['movings'];
    if ($datauser['lastdate'] < (time() - 300)) {
        $movings = 0;
        $sql .= " `sestime` = '" . time() . "', ";
    }
    if ($datauser['place'] != $headmod) {
        ++$movings;
        $sql .= " `place` = '" . $DB->toSql($headmod) . "', ";
    }
    if ($datauser['browser'] != $agn) {
        $sql .= " `browser` = '" . $DB->toSql($agn) . "', ";
    }
    $totalonsite = $datauser['total_on_site'];
    if ($datauser['lastdate'] > (time() - 300)) {
        $totalonsite = $totalonsite + time() - $datauser['lastdate'];
    }
    $DB->query("UPDATE `users` SET $sql
        `movings` = '$movings',
        `total_on_site` = '$totalonsite',
        `lastdate` = '" . time() . "'
        WHERE `id` = '$user_id'
    ");
} else {
    // Фиксируем местоположение гостей
    $movings = 0;
    $session = md5(core::$ip . core::$ip_via_proxy . core::$user_agent);
    $req = $DB->query("SELECT * FROM `cms_sessions` WHERE `session_id` = '$session' LIMIT 1");
    if ($DB->numRows($req)) {
        // Если есть в базе, то обновляем данные
        $res = $DB->getAssoc($req);
        $movings = ++$res['movings'];
        if ($res['sestime'] < (time() - 300)) {
            $movings = 1;
            $sql .= " `sestime` = '" . time() . "', ";
        }
        if ($res['place'] != $headmod) {
            $sql .= " `place` = '" . $DB->toSql($headmod) . "', ";
        }
        $DB->query("UPDATE `cms_sessions` SET $sql
            `movings` = '$movings',
            `lastdate` = '" . time() . "'
            WHERE `session_id` = '$session'
        ");
    } else {
        // Если еще небыло в базе, то добавляем запись
        $DB->query("INSERT INTO `cms_sessions` SET
            `session_id` = '" . $session . "',
            `ip` = '" . core::$ip . "',
            `ip_via_proxy` = '" . core::$ip_via_proxy . "',
            `browser` = '" . $DB->toSql($agn) . "',
            `lastdate` = '" . time() . "',
            `sestime` = '" . time() . "',
            `place` = '" . $DB->toSql($headmod) . "'
        ");
    }
}

/*
-----------------------------------------------------------------
Выводим сообщение о Бане
-----------------------------------------------------------------
*/
if (!empty($ban)) {
    echo '<div class="alarm">' . $lng['ban'] . '&#160;<a href="' . $set['homeurl'] . '/users/profile.php?act=ban">' . $lng['in_detail'] . '</a></div>';
}

/*
-----------------------------------------------------------------
Ссылки на непрочитанное
-----------------------------------------------------------------
*/
if ($user_id) {
    $list = array();
    $new_sys_mail = $DB->getCount($DB->query("SELECT COUNT(*) FROM `cms_mail` WHERE `from_id`='$user_id' AND `read`='0' AND `sys`='1' AND `delete`!='$user_id';"),
        0);
    if ($new_sys_mail) {
        $list[] = '<a href="' . $home . '/mail/index.php?act=systems">Система</a> (+' . $new_sys_mail . ')';
    }
    $new_mail = $DB->getCount($DB->query("SELECT COUNT(*) FROM `cms_mail` LEFT JOIN `cms_contact` ON `cms_mail`.`user_id`=`cms_contact`.`from_id` AND `cms_contact`.`user_id`='$user_id' WHERE `cms_mail`.`from_id`='$user_id' AND `cms_mail`.`sys`='0' AND `cms_mail`.`read`='0' AND `cms_mail`.`delete`!='$user_id' AND `cms_contact`.`ban`!='1' AND `cms_mail`.`spam`='0'"),
        0);
    if ($new_mail) {
        $list[] = '<a href="' . $home . '/mail/index.php?act=new">' . $lng['mail'] . '</a> (+' . $new_mail . ')';
    }
    if ($datauser['comm_count'] > $datauser['comm_old']) {
        $list[] = '<a href="' . core::$system_set['homeurl'] . '/users/profile.php?act=guestbook&amp;user=' . $user_id . '">' . $lng['guestbook'] . '</a> (' . ($datauser['comm_count'] - $datauser['comm_old']) . ')';
    }
    $new_album_comm = $DB->getCount($DB->query("SELECT COUNT(*) FROM `cms_album_files` WHERE `user_id` = '" . core::$user_id . "' AND `unread_comments` = 1"),
        0);
    if ($new_album_comm) {
        $list[] = '<a href="' . core::$system_set['homeurl'] . '/users/album.php?act=top&amp;mod=my_new_comm">' . $lng['albums_comments'] . '</a>';
    }

    if (!empty($list)) {
        echo '<div class="rmenu">' . $lng['unread'] . ': ' . functions::display_menu($list, ', ') . '</div>';
    }
}