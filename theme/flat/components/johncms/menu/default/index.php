<?php
/**
 * @var $this Component
 * @var $result
 * @var $params
 * @var $lang
 */
?>

<? foreach($result as $key=>$item): ?>
    <li<?= (!empty($item['active']) ? ' class="active"' : '') ?>>
        <a href="<?= $item['url'] ?>"><?= $item['name'] ?></a>
    </li>
<? endforeach; ?>
